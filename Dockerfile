FROM nginx:latest
EXPOSE 80
RUN pwd
COPY server.conf /etc/nginx/conf.d/default.conf
COPY dist/simpl-labs/browser /usr/share/nginx/html/
RUN mkdir /usr/share/nginx/html/slabs-conf