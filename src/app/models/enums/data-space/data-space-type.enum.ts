export enum DataSpaceType {
  EXPERIMENTATION = 'Experimentation',
  COMPLIANCE_TESTING = 'Compliance Testing',
  COLLABORATION = 'Collaboration',
  PRODUCTION_SIMULATION = 'Production Simulation',
}
