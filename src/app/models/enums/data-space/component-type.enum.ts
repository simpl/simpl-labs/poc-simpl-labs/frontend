export enum ComponentType {
  BE_COMPONENT = 'BE_COMPONENT',
  FE_COMPONENT = 'FE_COMPONENT',
}
