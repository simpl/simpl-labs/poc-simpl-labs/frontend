export enum SimplComponentStatus {
  BUILT = 'BUILT',
  UPLOADED = 'UPLOADED',
}
