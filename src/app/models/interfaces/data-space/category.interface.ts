import { CategoryType } from '../../enums/data-space/category-type.enum';
import { ComponentModel } from './component.interface';

export interface CategoryModel {
  id: number | null;
  type: CategoryType;
  componentList: ComponentModel[];
  name?: string;
}
