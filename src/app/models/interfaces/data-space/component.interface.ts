import { ComponentType } from '../../enums/data-space/component-type.enum';

export interface ComponentModel {
  id: number | null;
  type: ComponentType;
  param1: string; // Possible values will be 'DEBUG / INFO / WARNING';
  param2: string; // Default value will be '30000';
  param3: string; // Default value will be '/app/component/default.log';
  param4: string; // Default value will be '/app/component/configuration.properties';
  param5: string; // Default value will be '80';
  param6: string; // Default value will be 'default-uri';
  customImage: string | null;
  customComponentId: number | null;
}
