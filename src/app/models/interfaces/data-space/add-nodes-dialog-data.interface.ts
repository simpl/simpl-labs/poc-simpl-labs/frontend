import { NodeType } from '../../enums/data-space/node-type.enum';

export interface AddNodesDialogData {
  templateNodes: { name: string; type: NodeType }[];
}
