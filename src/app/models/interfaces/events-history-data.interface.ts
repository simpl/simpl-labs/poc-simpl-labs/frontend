export interface EventsHistoryData {
  date: string;
  event: {
    text: string;
    status: string;
  };
  path: string;
  details: string;
}
