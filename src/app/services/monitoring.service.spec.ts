import { TestBed } from '@angular/core/testing';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { HttpClient } from '@angular/common/http';
import { of } from 'rxjs';

import { MonitoringService } from './monitoring.service';
import { ConsumptionType } from '../models/enums/monitoring/consumption-type.enum';
import { TenantConsumption } from '../models/interfaces/monitoring/tenant-consumption.interface';
import { AppService } from './app.service';
import { envUrl } from '../../tests/env-url-mock';

const mockTenantConsumption: TenantConsumption = {
  name: ConsumptionType.CPU,
  unitOfMeasurement: 'cores',
  allocatable: 4,
  allocated: 3,
  used: 1.23,
  date: new Date().toString(),
};

describe('MonitoringService', () => {
  let monitoringService: MonitoringService;
  let httpClientSpy: jasmine.SpyObj<HttpClient>;
  let appServiceSpy: jasmine.SpyObj<AppService>;

  beforeEach(() => {
    const spy = jasmine.createSpyObj('HttpClient', ['get']);

    appServiceSpy = jasmine.createSpyObj('AppService', ['load']);
    Object.defineProperty(appServiceSpy, 'appUrls', {
      get: () => ({
        apiUrl: 'http://api-mock.org',
        simplUrl: '"http://simpl-mock.org/',
      }),
    });

    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule],
      providers: [
        MonitoringService,
        { provide: HttpClient, useValue: spy },
        { provide: AppService, useValue: appServiceSpy },
      ],
    });
    monitoringService = TestBed.inject(MonitoringService);
    httpClientSpy = TestBed.inject(HttpClient) as jasmine.SpyObj<HttpClient>;

    spyOnProperty(appServiceSpy, 'appUrls', 'get').and.returnValue(envUrl);
  });

  it('should be created', () => {
    expect(monitoringService).toBeTruthy();
  });

  it('#getTenantConsumption should return stubbed #tenantConsumption from a spy', () => {
    const stubTenantConsumption = of(mockTenantConsumption);
    httpClientSpy.get.and.returnValue(stubTenantConsumption);

    expect(monitoringService.getTenantConsumption(ConsumptionType.CPU))
      .withContext('service returned stub #tenantConsumption')
      .toBe(stubTenantConsumption);
    expect(httpClientSpy.get.calls.count())
      .withContext('spy method was called once')
      .toBe(1);
    expect(httpClientSpy.get.calls.mostRecent().returnValue).toBe(
      stubTenantConsumption,
    );
  });
});
