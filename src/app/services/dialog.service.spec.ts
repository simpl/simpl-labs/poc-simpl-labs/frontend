import { TestBed } from '@angular/core/testing';
import { MatDialog } from '@angular/material/dialog';

import { DialogService } from './dialog.service';

class DialogRefMock {
  close() {}
}

class DialogMock {
  open() {
    return new DialogRefMock();
  }
}

describe('DialogService', () => {
  let service: DialogService;

  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [{ provide: MatDialog, useClass: DialogMock }],
    });
    service = TestBed.inject(DialogService);
    service.dialog = TestBed.inject(MatDialog);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });

  it('should open WarningDialogComponent', () => {
    spyOn(service.dialog, 'open');
    service.openWarningDialog({
      content: 'content-mock',
      title: 'title-mock',
    });
    expect(service.dialog.open).toHaveBeenCalled();
  });

  it('should open ErrorDialogComponent', () => {
    spyOn(service.dialog, 'open');
    service.openErrorDialog({
      content: 'content-mock',
      title: 'title-mock',
    });
    expect(service.dialog.open).toHaveBeenCalled();
  });
});
