import { TestBed } from '@angular/core/testing';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { Subject } from 'rxjs';

import { TreeMenuService } from './tree-menu.service';
import { DataSpaceService } from './data-space.service';
import { DataSpaceModel } from '../models/interfaces/data-space/data-space.interface';
import { dataSpacesMock } from '../../mocks/data-space-mock';
import { NodeModel } from '../models/interfaces/data-space/node.interface';
import { slabsFlatNodesMock } from '../../tests/data-spaces/graph-chart-datum-mock';
import { GraphChartDatum } from '../models/interfaces/graph-chart-datum.interface';
import { TemplateService } from './template.service';
import { TemplateModel } from '../models/interfaces/template.interface';
import { basicTemplatesMock } from '../components/wizard/constants/data-space-creation.data';

const mockNodeList: NodeModel[] = dataSpacesMock[1].nodeList;

const nodeMock: GraphChartDatum = {
  id: mockNodeList[1].id?.toString(),
  name: mockNodeList[1].name,
  type: mockNodeList[1].type,
  collapsed: true,
  x: 0,
  y: 0,
};

describe('TreeMenuService', () => {
  let service: TreeMenuService;
  let dataSpaceServiceSpy: jasmine.SpyObj<DataSpaceService>;
  let templateServiceSpy: jasmine.SpyObj<TemplateService>;

  beforeEach(() => {
    dataSpaceServiceSpy = jasmine.createSpyObj('DataSpaceService', [
      'updateNewDataSpaceNodeList',
    ]);
    templateServiceSpy = jasmine.createSpyObj('TemplateService', [
      'getBasicTemplates',
      'getAllTemplates',
    ]);

    dataSpaceServiceSpy.newDataSpace = dataSpacesMock[1];
    dataSpaceServiceSpy.newDataSpaceSubject$ = new Subject<DataSpaceModel>();
    templateServiceSpy.viewedTemplateSubject$ = new Subject<TemplateModel>();

    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule],
      providers: [
        { provide: DataSpaceService, useValue: dataSpaceServiceSpy },
        { provide: TemplateService, useValue: templateServiceSpy },
      ],
    });
    service = TestBed.inject(TreeMenuService);

    service.inputFieldShown = false;
    service.breadcrumbs = slabsFlatNodesMock;
    service.dataSpaceBreadCrumb = {
      name: '',
      expandable: true,
      level: -1,
      type: '',
      expanded: true,
      id: null!,
    };
    spyOn(service, 'createBreadcrumbs').and.returnValue(slabsFlatNodesMock);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });

  it('should rename a node', () => {
    service.renameNode(nodeMock, mockNodeList, 'new-name-mock');
    expect(dataSpaceServiceSpy.updateNewDataSpaceNodeList).toHaveBeenCalled();
    expect(dataSpaceServiceSpy.newDataSpace.nodeList).toEqual(mockNodeList);
    expect(service.inputFieldShown).toBeFalse();
  });

  it('should duplicate a node', () => {
    service.duplicateNode(nodeMock, mockNodeList);
    expect(dataSpaceServiceSpy.updateNewDataSpaceNodeList).toHaveBeenCalled();
    expect(dataSpaceServiceSpy.newDataSpace.nodeList).toEqual(mockNodeList);
  });

  it('should delete a node', () => {
    service.deleteNode(nodeMock, mockNodeList);
    expect(dataSpaceServiceSpy.updateNewDataSpaceNodeList).toHaveBeenCalled();
    expect(dataSpaceServiceSpy.newDataSpace.nodeList).toEqual(mockNodeList);
  });

  it('should call #updateBreadcrumbs()', () => {
    service.updateBreadcrumbs(slabsFlatNodesMock[0], false);
    expect(service.createBreadcrumbs).toHaveBeenCalled();
  });

  it('should #createBreadcrumbs()', () => {
    const idx = slabsFlatNodesMock[1].level + 1;
    const len = slabsFlatNodesMock.length;
    let _breadcrumbs = [...slabsFlatNodesMock];
    _breadcrumbs.splice(idx, len - idx);
    _breadcrumbs.push(slabsFlatNodesMock[1]);
    service.createBreadcrumbs(
      service.breadcrumbs,
      { ...slabsFlatNodesMock[1], expandable: true, expanded: true },
      false,
    );
    expect(service.breadcrumbs).toEqual(_breadcrumbs);
  });

  it('should set #dataSpaceBreadCrumb.name', () => {
    templateServiceSpy.viewedTemplateSubject$.next(basicTemplatesMock[0]);
    expect(service.dataSpaceBreadCrumb.name).toBe(
      basicTemplatesMock[0].dataSpace.name,
    );
    expect(service.breadcrumbs).toEqual([service.dataSpaceBreadCrumb]);
  });
});
