import {
  HttpEvent,
  HttpHandler,
  HttpInterceptor,
  HttpRequest,
  HttpResponse,
} from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable, of } from 'rxjs';
import { delay } from 'rxjs/operators';

import { urlResponse } from './url-response';
import { monitoringIntercept } from './monitoring-intercept';
import { customComponentsIntercept } from './custom-components-intercept';

@Injectable({
  providedIn: 'root',
})
export class MockInterceptor implements HttpInterceptor {
  intercept(
    request: HttpRequest<any>,
    next: HttpHandler,
  ): Observable<HttpResponse<any>> | Observable<HttpEvent<any>> {
    for (const element of urlResponse) {
      if (request.urlWithParams.indexOf(element.url) >= 0) {
        let response = of(
          new HttpResponse({ status: 200, body: element.json }),
        ).pipe(delay(50));

        if (element.url.includes('system-monitoring/'))
          response = monitoringIntercept(element);
        else if (element.url.includes('custom-components/'))
          response = customComponentsIntercept(element, request);

        console.info(`Mock response loaded from: ${request.urlWithParams}`);

        return response;
      }
    }
    return next.handle(request);
  }
}
