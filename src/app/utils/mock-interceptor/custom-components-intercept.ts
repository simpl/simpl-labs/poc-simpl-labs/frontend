import { HttpRequest, HttpResponse } from '@angular/common/http';
import { Observable, of, delay } from 'rxjs';

import { dataSpacesMock } from '../../../mocks/data-space-mock';
import {
  testExecutionStatus1Mock,
  testExecutionStatus2Mock,
  testExecutionStatus3Mock,
} from '../../../mocks/test-execution-status-mock';
import { TestCaseStatus } from '../../models/enums/test-case-status.enum';
import { slabsCustomComponentsMock } from '../../../mocks/slabs-custom-component-mock';
import {
  testCasesSuite1Mock,
  testCasesSuite2Mock,
  testCasesSuite3Mock,
  testCasesSuite4Mock,
  testCasesSuite5Mock,
} from '../../../mocks/test-cases-mock';

export const customComponentsIntercept = (
  element: { url: string; json: any },
  request: HttpRequest<any>,
): Observable<HttpResponse<any>> => {
  const comp1TestCaseList = testCasesSuite1Mock.concat(
    testCasesSuite2Mock.slice(0, testCasesSuite2Mock.length - 1),
  );
  const comp2TestCaseList = testCasesSuite3Mock.concat(
    testCasesSuite4Mock.slice(0, testCasesSuite4Mock.length - 1),
  );
  const comp3TestCaseList = testCasesSuite5Mock.slice(
    0,
    testCasesSuite5Mock.length - 1,
  );

  const counters = [
    {
      compId: slabsCustomComponentsMock[0].id,
      compTime: 0,
    },
    {
      compId: slabsCustomComponentsMock[1].id,
      compTime: 0,
    },
    {
      compId: slabsCustomComponentsMock[2].id,
      compTime: 0,
    },
  ];

  const resetCounter = (id: number) =>
    counters.forEach((c) => (c.compTime = c.compId === id ? 0 : c.compTime));

  if (element.url.includes('custom-components/tests/')) {
    if (request.method === 'POST') {
      if (element.url.endsWith('start')) {
        resetCounter(+request.body.customComponentId);
      } else if (element.url.endsWith('stop')) {
        element.json.stopTime = new Date().toString();

        testExecutionStatus1Mock.testExecution.status =
          TestCaseStatus.UNDEFINED;
        testExecutionStatus2Mock.testExecution.status =
          TestCaseStatus.UNDEFINED;
        testExecutionStatus3Mock.testExecution.status =
          TestCaseStatus.UNDEFINED;

        comp1TestCaseList.forEach((x) => (x.status = TestCaseStatus.UNDEFINED));
        comp2TestCaseList.forEach((x) => (x.status = TestCaseStatus.UNDEFINED));
        comp3TestCaseList.forEach((x) => (x.status = TestCaseStatus.UNDEFINED));
      }
    } else if (request.method === 'GET' && element.url.endsWith('dataspaces')) {
      return of(new HttpResponse({ status: 200, body: dataSpacesMock })).pipe(
        delay(700),
      );
    } else if (request.method === 'GET' && element.url.includes(`status`)) {
      counters.forEach(
        (c) =>
          (c.compTime =
            c.compId === +element.url.charAt(element.url.length - 1)
              ? c.compTime + 1
              : c.compTime),
      );
      counters.forEach((counter, i) => {
        if (counter.compTime === 1) {
          element.json.testExecution.startDate = new Date().toString();
          element.json.testExecution.endDate = null!;
          if (counter.compId === 1 && element.url.endsWith(`/1/1`))
            comp1TestCaseList.forEach((x) => {
              x.endDate = null!;
              x.status = TestCaseStatus.UNDEFINED;
            });
          if (counter.compId === 2 && element.url.endsWith(`/2/2`))
            comp2TestCaseList.forEach((x) => {
              x.endDate = null!;
              x.status = TestCaseStatus.UNDEFINED;
            });
          if (counter.compId === 3 && element.url.endsWith(`/3/3`))
            comp3TestCaseList.forEach((x) => {
              x.endDate = null!;
              x.status = TestCaseStatus.UNDEFINED;
            });
        } else if (counter.compTime === 5) {
          if (counter.compId === 1 && element.url.endsWith(`/1/1`)) {
            comp1TestCaseList.forEach((x, i, a) => {
              x.startDate =
                i < a.length / 2 ? new Date().toString() : x.startDate;
              x.status = i < a.length / 2 ? TestCaseStatus.ONGOING : x.status;
            });
            testExecutionStatus1Mock.testExecution.passedTestsNumber =
              comp1TestCaseList.filter(
                (tc) => tc.status === TestCaseStatus.SUCCESS,
              ).length;
            testExecutionStatus1Mock.testExecution.status =
              TestCaseStatus.ONGOING;
          } else if (counter.compId === 2 && element.url.endsWith(`/2/2`)) {
            comp2TestCaseList.forEach((x, i, a) => {
              x.startDate =
                i < a.length / 2 ? new Date().toString() : x.startDate;
              x.status = i < a.length / 2 ? TestCaseStatus.ONGOING : x.status;
            });
            testExecutionStatus2Mock.testExecution.passedTestsNumber =
              comp2TestCaseList.filter(
                (tc) => tc.status === TestCaseStatus.SUCCESS,
              ).length;
            testExecutionStatus2Mock.testExecution.status =
              TestCaseStatus.ONGOING;
          } else if (counter.compId === 3 && element.url.endsWith(`/3/3`)) {
            comp3TestCaseList.forEach((x, i, a) => {
              x.startDate =
                i < a.length / 2 ? new Date().toString() : x.startDate;
              x.status = i < a.length / 2 ? TestCaseStatus.ONGOING : x.status;
            });
            testExecutionStatus3Mock.testExecution.passedTestsNumber =
              comp3TestCaseList.filter(
                (tc) => tc.status === TestCaseStatus.SUCCESS,
              ).length;
            testExecutionStatus3Mock.testExecution.status =
              TestCaseStatus.ONGOING;
          }
        } else if (counter.compTime === 10) {
          if (counter.compId === 1 && element.url.endsWith(`/1/1`)) {
            comp1TestCaseList.forEach((x, i, a) => {
              x.startDate =
                i >= a.length / 2 ? new Date().toString() : x.startDate;
              x.status = i >= a.length / 2 ? TestCaseStatus.ONGOING : x.status;
            });
          } else if (counter.compId === 2 && element.url.endsWith(`/2/2`)) {
            comp2TestCaseList.forEach((x, i, a) => {
              x.startDate =
                i >= a.length / 2 ? new Date().toString() : x.startDate;
              x.status = i >= a.length / 2 ? TestCaseStatus.ONGOING : x.status;
            });
          } else if (counter.compId === 3 && element.url.endsWith(`/3/3`)) {
            comp3TestCaseList.forEach((x, i, a) => {
              x.startDate =
                i >= a.length / 2 ? new Date().toString() : x.startDate;
              x.status = i >= a.length / 2 ? TestCaseStatus.ONGOING : x.status;
            });
          }
        } else if (counter.compTime === 13) {
          if (counter.compId === 1 && element.url.endsWith(`/1/1`)) {
            comp1TestCaseList.forEach((x, i, a) => {
              x.endDate = new Date().toString();
              x.status =
                i < a.length / 2
                  ? TestCaseStatus.SUCCESS
                  : TestCaseStatus.FAILURE;
            });
            testExecutionStatus1Mock.testExecution.status =
              comp1TestCaseList.every(
                (x) => x.status === TestCaseStatus.SUCCESS,
              )
                ? TestCaseStatus.SUCCESS
                : comp1TestCaseList.some(
                      (x) => x.status === TestCaseStatus.FAILURE,
                    )
                  ? TestCaseStatus.FAILURE
                  : testExecutionStatus1Mock.testExecution.status;
            testExecutionStatus1Mock.testExecution.passedTestsNumber =
              comp1TestCaseList.filter(
                (tc) => tc.status === TestCaseStatus.SUCCESS,
              ).length;
            testExecutionStatus1Mock.testExecution.totalTestsNumber =
              comp1TestCaseList.length;
          } else if (counter.compId === 2 && element.url.endsWith(`/2/2`)) {
            comp2TestCaseList.forEach((x, i, a) => {
              x.endDate = new Date().toString();
              x.status =
                i < a.length / 2
                  ? TestCaseStatus.SUCCESS
                  : TestCaseStatus.FAILURE;
            });
            testExecutionStatus2Mock.testExecution.status =
              comp2TestCaseList.every(
                (x) => x.status === TestCaseStatus.SUCCESS,
              )
                ? TestCaseStatus.SUCCESS
                : comp2TestCaseList.some(
                      (x) => x.status === TestCaseStatus.FAILURE,
                    )
                  ? TestCaseStatus.FAILURE
                  : testExecutionStatus2Mock.testExecution.status;
            testExecutionStatus2Mock.testExecution.passedTestsNumber =
              comp2TestCaseList.filter(
                (tc) => tc.status === TestCaseStatus.SUCCESS,
              ).length;
            testExecutionStatus2Mock.testExecution.totalTestsNumber =
              comp2TestCaseList.length;
          } else if (counter.compId === 3 && element.url.endsWith(`/3/3`)) {
            comp3TestCaseList.forEach((x, i, a) => {
              x.endDate = new Date().toString();
              x.status =
                i < a.length / 2
                  ? TestCaseStatus.SUCCESS
                  : TestCaseStatus.FAILURE;
            });
            testExecutionStatus3Mock.testExecution.status =
              comp3TestCaseList.every(
                (x) => x.status === TestCaseStatus.SUCCESS,
              )
                ? TestCaseStatus.SUCCESS
                : comp3TestCaseList.some(
                      (x) => x.status === TestCaseStatus.FAILURE,
                    )
                  ? TestCaseStatus.FAILURE
                  : testExecutionStatus3Mock.testExecution.status;
            testExecutionStatus3Mock.testExecution.passedTestsNumber =
              comp3TestCaseList.filter(
                (tc) => tc.status === TestCaseStatus.SUCCESS,
              ).length;
            testExecutionStatus3Mock.testExecution.totalTestsNumber =
              comp3TestCaseList.length;
          }
          element.json.testExecution.endDate = new Date().toString();
        }
      });
    }
  }

  return of(new HttpResponse({ status: 200, body: element.json })).pipe(
    delay(50),
  );
};
