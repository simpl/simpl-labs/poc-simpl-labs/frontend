import { currentConsumptionsMock } from '../../../mocks/current-consumption-mock';
import { customComponentHistoryMock } from '../../../mocks/custom-component-history-mock';
import { dataSpacesMock } from '../../../mocks/data-space-mock';
import { sessionStartResponseMock } from '../../../mocks/session-start-response-mock';
import { sessionStopResponseMock } from '../../../mocks/session-stop-response-mock';
import { slabsCustomComponentsMock } from '../../../mocks/slabs-custom-component-mock';
import {
  testCasesSuite1Mock,
  testCasesSuite2Mock,
  testCasesSuite3Mock,
  testCasesSuite4Mock,
  testCasesSuite5Mock,
} from '../../../mocks/test-cases-mock';
import {
  testExecutionStatus1Mock,
  testExecutionStatus2Mock,
  testExecutionStatus3Mock,
} from '../../../mocks/test-execution-status-mock';
import {
  testSuites1Mock,
  testSuites2Mock,
  testSuites3Mock,
} from '../../../mocks/test-suites-mock';
import {
  basicTemplatesMock,
  allTemplatesMock,
} from '../../components/wizard/constants/data-space-creation.data';

export const urlResponse: { url: string; json: any }[] = [
  {
    url: 'basic-templates',
    json: basicTemplatesMock,
  },
  {
    url: 'all-templates',
    json: allTemplatesMock,
  },
  // {
  //   url: 'system-monitoring/component/historical/cpu',
  //   json: null,
  // },
  // {
  //   url: 'system-monitoring/component/historical/memory',
  //   json: null,
  // },
  // {
  //   url: 'system-monitoring/component/historical/storage',
  //   json: null,
  // },
  // {
  //   url: 'system-monitoring/dataspace/current/all',
  //   json: currentConsumptionsMock,
  // },
  // {
  //   url: 'system-monitoring/dataspace/current/1',
  //   json: currentConsumptionsMock,
  // },
  // {
  //   url: 'system-monitoring/dataspace/current/2',
  //   json: currentConsumptionsMock,
  // },
  // {
  //   url: 'system-monitoring/general/historical/cpu',
  //   json: null,
  // },
  // {
  //   url: 'system-monitoring/general/historical/memory',
  //   json: null,
  // },
  // {
  //   url: 'system-monitoring/general/historical/storage',
  //   json: null,
  // },
  // {
  //   url: 'system-monitoring/general/current/cpu',
  //   json: null,
  // },
  // {
  //   url: 'system-monitoring/general/current/memory',
  //   json: null,
  // },
  // {
  //   url: 'system-monitoring/general/current/storage',
  //   json: null,
  // },
  // {
  //   url: 'system-monitoring/general/current',
  //   json: currentConsumptionsMock,
  // },
  // {
  //   url: 'dataspaces/1',
  //   json: dataSpacesMock[0],
  // },
  // {
  //   url: 'dataspaces/2',
  //   json: dataSpacesMock[1],
  // },
  // {
  //   url: 'dataspaces',
  //   json: dataSpacesMock,
  // },
  // {
  //   url: `custom-components/tests/status/1/1`,
  //   json: testExecutionStatus1Mock,
  // },
  // {
  //   url: `custom-components/tests/status/2/2`,
  //   json: testExecutionStatus2Mock,
  // },
  // {
  //   url: `custom-components/tests/status/3/3`,
  //   json: testExecutionStatus3Mock,
  // },
  // {
  //   url: `custom-components/tests/start`,
  //   json: sessionStartResponseMock,
  // },
  // {
  //   url: `custom-components/tests/stop`,
  //   json: sessionStopResponseMock,
  // },
  // {
  //   url: `custom-components/test-suites/1/1`,
  //   json: testSuites1Mock,
  // },
  // {
  //   url: `custom-components/test-suites/2/2`,
  //   json: testSuites2Mock,
  // },
  // {
  //   url: `custom-components/test-suites/3/3`,
  //   json: testSuites3Mock,
  // },
  // {
  //   url: `custom-components/test-cases/1`,
  //   json: testCasesSuite1Mock,
  // },
  // {
  //   url: `custom-components/test-cases/2`,
  //   json: testCasesSuite2Mock,
  // },
  // {
  //   url: `custom-components/test-cases/3`,
  //   json: testCasesSuite3Mock,
  // },
  // {
  //   url: `custom-components/test-cases/4`,
  //   json: testCasesSuite4Mock,
  // },
  // {
  //   url: `custom-components/test-cases/5`,
  //   json: testCasesSuite5Mock,
  // },
  // {
  //   url: `custom-components/tests-history`,
  //   json: customComponentHistoryMock,
  // },
  // {
  //   url: 'custom-components',
  //   json: slabsCustomComponentsMock,
  // },
];
