import { Injectable } from '@angular/core';
import {
  HttpRequest,
  HttpHandler,
  HttpEvent,
  HttpInterceptor,
  HttpResponse,
} from '@angular/common/http';
import { Observable } from 'rxjs';
import { catchError, delay, map } from 'rxjs/operators';

import { LoadingService } from '../services/loading.service';

/**
 * This class is for intercepting http requests. When a request starts, we set the loadingSub property
 * in the LoadingService to true. Once the request completes and we have a response, set the loadingSub
 * property to false. If an error occurs while servicing the request, set the loadingSub property to false.
 * @class {LoadingSpinnerInterceptor}
 */
@Injectable()
export class LoadingSpinnerInterceptor implements HttpInterceptor {
  constructor(private readonly _loadingService: LoadingService) {}

  delay2False(url: string): number {
    return url.includes('tests/status') &&
      (this._loadingService.itbTimer === 0 ||
        this._loadingService.storedUrls.some((st) => st.includes('tests/stop')))
      ? 3000
      : 0;
  }

  intercept(
    request: HttpRequest<any>,
    next: HttpHandler,
  ): Observable<HttpEvent<any>> {
    this._loadingService.setLoading(
      (this._loadingService.monitoringTimer === 0 &&
        request.url.endsWith('current/cpu')) ||
        (request.url.endsWith('dataspaces') &&
          this._loadingService.timer === 0) ||
        (request.url.endsWith('custom-components') &&
          this._loadingService.itbTimer === 0) ||
        (request.url.includes('custom-components/test-suites') &&
          this._loadingService.itbTimer === 0) ||
        (request.url.includes('tests/status') &&
          (this._loadingService.itbTimer === 0 ||
            this._loadingService.storedUrls.some((st) =>
              st.includes('tests/stop'),
            ))) ||
        request.url.endsWith('templates') ||
        request.method === 'POST',
      request.url,
    );

    return next
      .handle(request)
      .pipe(
        catchError((err) => {
          this._loadingService.setLoading(false, request.url);
          return err;
        }),
      )
      .pipe(
        delay(this.delay2False(request.url)),
        map<any, HttpEvent<any>>((evt: any) => {
          if (evt instanceof HttpResponse)
            if (
              !request.url.endsWith('/tests/start') &&
              !request.url.endsWith('/tests/stop')
            )
              this._loadingService.setLoading(false, request.url);
          return evt;
        }),
      );
  }
}
