import { Component, OnDestroy, OnInit } from '@angular/core';
import { RouterOutlet } from '@angular/router';
import { CommonModule } from '@angular/common';
import { delay, Subscription } from 'rxjs';
import { MatSidenavModule } from '@angular/material/sidenav';
import { MatIconModule } from '@angular/material/icon';
import { MatButtonModule } from '@angular/material/button';
import { MatToolbarModule } from '@angular/material/toolbar';
import { MatListModule } from '@angular/material/list';

import { HeaderComponent } from './components/_shared/header/header.component';
import { NavigationListComponent } from './components/_shared/navigation-list/navigation-list.component';
import { KeycloakOperationService } from './services/keycloak-operation.service';
import { DataSpaceService } from './services/data-space.service';
import { User } from './models/interfaces/user.interface';
import { LoadingService } from './services/loading.service';
import { LoadingSpinnerComponent } from './components/_shared/loading-spinner/loading-spinner.component';
import { ITBTestsExecutionSnackbarComponent } from './components/conformance-tests/itb-tests-execution-snackbar/itb-tests-execution-snackbar.component';
import { SimplLabsLogoComponent } from './components/_shared/simpl-labs-logo/simpl-labs-logo.component';

@Component({
  selector: 'app-root',
  standalone: true,
  templateUrl: './app.component.html',
  styleUrl: './app.component.scss',
  imports: [
    RouterOutlet,
    CommonModule,
    MatSidenavModule,
    MatIconModule,
    MatButtonModule,
    MatToolbarModule,
    MatListModule,
    HeaderComponent,
    NavigationListComponent,
    LoadingSpinnerComponent,
    ITBTestsExecutionSnackbarComponent,
    SimplLabsLogoComponent,
  ],
})
export class AppComponent implements OnInit, OnDestroy {
  title = 'Simpl-Labs';
  sideNavOpened = true;
  user: User = {
    email: '',
    name: '',
    username: '',
    id: null!,
  };

  loading = false;
  dataSpacesNumber!: number;

  loadingSub = new Subscription();

  constructor(
    private readonly dataSpaceService: DataSpaceService,
    private readonly keycloakOperationService: KeycloakOperationService,
    private readonly _loading: LoadingService,
  ) {}

  ngOnInit(): void {
    this.listenToLoading();

    this.dataSpacesNumber = this.dataSpaceService.getDataSpacesNumber();

    this.keycloakOperationService.getUserProfile().then((profile) => {
      if (profile)
        this.user = {
          email: profile.email!,
          name: profile.firstName! + ' ' + profile.lastName!,
          username: profile.username!,
          id: +profile.id!,
        };
    });
  }

  ngOnDestroy(): void {
    this.loadingSub.unsubscribe();
  }

  logout() {
    this.keycloakOperationService.logout();
  }

  listenToLoading(): void {
    this.loadingSub = this._loading.loadingSubject$
      .pipe(delay(0))
      .subscribe((loading) => (this.loading = loading));
  }
}
