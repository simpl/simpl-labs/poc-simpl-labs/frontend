export const ms2HHMMSS = (ms: number): string => {
  const padWithZero = (n: number): string => (n < 10 ? `0${n}` : `${n}`);

  // 1- Convert to seconds:
  let seconds: string | number = ms / 1000;

  // 2- Extract hours:
  let hours: string | number = parseInt((seconds / 3600).toString()); // 3,600 seconds in 1 hour
  hours = padWithZero(hours);

  seconds = seconds % 3600; // seconds remaining after extracting hours

  // 3- Extract minutes:
  let minutes: string | number = parseInt((seconds / 60).toString()); // 60 seconds in 1 minute
  minutes = padWithZero(minutes);

  // 4- Keep only seconds not extracted to minutes:
  seconds = Math.round(seconds % 60);
  seconds = padWithZero(seconds);

  return `${hours}:${minutes}:${seconds}`;
};
