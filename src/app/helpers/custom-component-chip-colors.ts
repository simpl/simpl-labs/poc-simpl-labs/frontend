import { ThemePalette } from '@angular/material/core';

interface StatusChipColor {
  status: string;
  color: ThemePalette | 'undefined';
}

export const customComponentChipColors: StatusChipColor[] = [
  { status: 'SUCCESS', color: 'primary' },
  { status: 'FAILURE', color: 'warn' },
  { status: 'ONGOING', color: 'accent' },
  { status: 'UNDEFINED', color: 'undefined' },
];

const mapStatus2ChipColor = new Map<string, ThemePalette | 'undefined'>();

customComponentChipColors.forEach((_, i) =>
  mapStatus2ChipColor.set(
    customComponentChipColors[i].status.toUpperCase(),
    customComponentChipColors[i].color,
  ),
);

export default mapStatus2ChipColor;
