import { Pipe, PipeTransform } from '@angular/core';
import { titlecaseNodeNameFcn } from '../helpers/titlecase-node-name.function';

@Pipe({
  name: 'titlecaseNodeName',
  standalone: true,
})
export class TitlecaseNodeName implements PipeTransform {
  transform(value: string, ...args: boolean[]): string {
    return titlecaseNodeNameFcn(value, args[0]);
  }
}
