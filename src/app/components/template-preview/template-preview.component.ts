import { Component, OnDestroy, OnInit, ViewChild } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ActivatedRoute, NavigationEnd, Router } from '@angular/router';
import { MatTabsModule } from '@angular/material/tabs';
import { MatIconModule } from '@angular/material/icon';
import { MatButtonModule } from '@angular/material/button';
import { MatToolbarModule } from '@angular/material/toolbar';
import { Subscription, map } from 'rxjs';
import { MatChipsModule } from '@angular/material/chips';
import { MatFormFieldModule } from '@angular/material/form-field';
import { FormsModule } from '@angular/forms';
import { MatInputModule } from '@angular/material/input';
import { MatTableDataSource, MatTableModule } from '@angular/material/table';

import { TemplateModel } from '../../models/interfaces/template.interface';
import { DataSpaceGraphChartComponent } from '../data-space-graph-chart/data-space-graph-chart.component';
import { TemplateService } from '../../services/template.service';
import { ContextTreeMenuComponent } from '../_shared/context-tree-menu/context-tree-menu.component';
import { SLabsFlatNode } from '../../models/interfaces/tree-node.interface';
import { NodeModel } from '../../models/interfaces/data-space/node.interface';
import { TreeMenuService } from '../../services/tree-menu.service';
import { chartNode2SlabsFlatNode } from '../../helpers/chart-node-2-slabs-flat-node.map';
import { BreadcrumbComponent } from '../_shared/breadcrumb/breadcrumb.component';
import { CategoryComponentsTableComponent } from '../_shared/category-components-table/category-components-table.component';
import { CategoryModel } from '../../models/interfaces/data-space/category.interface';
import { CategoryType } from '../../models/enums/data-space/category-type.enum';
import { ComponentModel } from '../../models/interfaces/data-space/component.interface';
import { MatDrawer, MatSidenavModule } from '@angular/material/sidenav';
import { GraphChartDatum } from '../../models/interfaces/graph-chart-datum.interface';
import { DataSpaceModel } from '../../models/interfaces/data-space/data-space.interface';
import { DrawerService } from '../../services/drawer.service';
import { DataSpaceService } from '../../services/data-space.service';
import { PageContentType } from '../../models/enums/data-space/page-content-type.enum';
import { ComponentInfo } from '../../models/interfaces/data-space/component-info.interface';
import { ComponentMoreInfoComponent } from '../wizard/component-more-info/component-more-info.component';

@Component({
  selector: 'app-template-preview',
  standalone: true,
  templateUrl: './template-preview.component.html',
  styleUrl: './template-preview.component.scss',
  imports: [
    CommonModule,
    FormsModule,
    MatTabsModule,
    MatButtonModule,
    MatIconModule,
    MatToolbarModule,
    MatChipsModule,
    MatFormFieldModule,
    MatInputModule,
    MatTableModule,
    MatSidenavModule,
    DataSpaceGraphChartComponent,
    ContextTreeMenuComponent,
    BreadcrumbComponent,
    CategoryComponentsTableComponent,
    ComponentMoreInfoComponent,
  ],
})
export class TemplatePreviewComponent implements OnInit, OnDestroy {
  @ViewChild('componentInfoDrawer', { static: true })
  componentInfoDrawer!: MatDrawer;

  previewTemplate!: TemplateModel;
  dataSpace!: DataSpaceModel;
  _nodeList!: NodeModel[];
  isTemplatePreview = false;

  searchValue = '';

  infoTableDataSource = new MatTableDataSource<{
    property: string;
    value: string | string[];
  }>();
  infoTableColumns: string[] = ['property', 'value'];

  pageContentType = PageContentType.GRAPH_CHART.toString();
  pageContentTypes = PageContentType;

  chartNode!: NodeModel;
  tableCategory!: CategoryModel;
  componentInfo!: ComponentInfo;

  routeParamMapSub = new Subscription();
  routerEventsSub = new Subscription();
  treeMenuNodeSubscription = new Subscription();
  graphChartNodeSubscription = new Subscription();

  constructor(
    private readonly activatedRoute: ActivatedRoute,
    private readonly treeMenuService: TreeMenuService,
    private readonly templateService: TemplateService,
    private readonly router: Router,
    private readonly drawerService: DrawerService,
    private readonly dataSpaceService: DataSpaceService,
  ) {
    this.routerEventsSub = this.router.events.subscribe((event) => {
      if (event instanceof NavigationEnd)
        this.isTemplatePreview = event.url.includes('template-preview');
    });
  }

  ngOnInit(): void {
    this.routeParamMapSub = this.activatedRoute.paramMap
      .pipe(map(() => window.history.state.template))
      .subscribe((template: TemplateModel) => {
        if (template) {
          this.previewTemplate = template;
          this.templateService.viewedTemplateSubject$.next(
            this.previewTemplate,
          );
          this.getInfoTableData(this.previewTemplate);
          this.dataSpace = this.previewTemplate.dataSpace;
          this._nodeList = [...this.previewTemplate.dataSpace.nodeList];
        }
      });

    this.treeMenuNodeSubscription = this.treeMenuService.treeMenuNodeSubject$
      .asObservable()
      .subscribe((treeFlatNode) => this.updatePageContent(treeFlatNode));

    this.graphChartNodeSubscription =
      this.treeMenuService.graphChartNodeSubject$
        .asObservable()
        .subscribe((graphChartDatum: GraphChartDatum) =>
          this.updatePageContent(chartNode2SlabsFlatNode(graphChartDatum)),
        );
  }

  ngOnDestroy(): void {
    this.routeParamMapSub.unsubscribe();
    this.routerEventsSub.unsubscribe();
    this.treeMenuNodeSubscription.unsubscribe();
    this.graphChartNodeSubscription.unsubscribe();
  }

  getInfoTableData(template: TemplateModel = this.previewTemplate) {
    const props = ['author', 'lastUpdate', 'license', 'tags'];

    this.infoTableDataSource = new MatTableDataSource(
      this.dataSpaceService.createInfoTableData(template, props),
    );
  }

  updatePageContent(treeFlatNode: SLabsFlatNode) {
    const pageContentUpdate = this.dataSpaceService.updatePageContent(
      treeFlatNode,
      this.dataSpace,
      this._nodeList,
      this.pageContentType,
      this.tableCategory,
    );

    this.dataSpace = pageContentUpdate.dataSpace;
    this._nodeList = pageContentUpdate.nodeList;
    this.pageContentType = pageContentUpdate.pageContentType;
    this.tableCategory = pageContentUpdate.tableCategory;
  }

  openComponentInfoDrawer(event: {
    categoryType: CategoryType;
    comp: ComponentModel;
  }) {
    this.componentInfo = event.comp as ComponentInfo;
    this.componentInfo.categoryType = event.categoryType;

    this.drawerService.openComponentDrawer(this.componentInfoDrawer);
  }

  closeComponentInfoDrawer() {
    this.drawerService.closeComponentDrawer(this.componentInfoDrawer);
  }
}
