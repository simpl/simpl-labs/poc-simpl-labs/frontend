import { ComponentFixture, TestBed } from '@angular/core/testing';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material/dialog';
import { HttpClientTestingModule } from '@angular/common/http/testing';

import { ChangeComponentDialogComponent } from './change-component-dialog.component';
import { slabsCustomComponentsMock } from '../../../../mocks/slabs-custom-component-mock';
import { ComponentModel } from '../../../models/interfaces/data-space/component.interface';
import { dataSpacesMock } from '../../../../mocks/data-space-mock';

const dialogDataMock: ComponentModel =
  dataSpacesMock[0].nodeList[0].categoryList[0].componentList[0];

const dialogRefMock = {
  close: () => {},
};

describe('ChangeComponentDialogComponent', () => {
  let component: ChangeComponentDialogComponent;
  let fixture: ComponentFixture<ChangeComponentDialogComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports: [ChangeComponentDialogComponent, HttpClientTestingModule],
      providers: [
        { provide: MAT_DIALOG_DATA, useValue: dialogDataMock },
        { provide: MatDialogRef, useValue: dialogRefMock },
      ],
    }).compileComponents();

    fixture = TestBed.createComponent(ChangeComponentDialogComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should select the component', () => {
    component.selectComponent(slabsCustomComponentsMock[0]);
    expect(component.selectedComponent).toEqual(slabsCustomComponentsMock[0]);
  });

  it('should change the component', () => {
    spyOn(dialogRefMock, 'close');
    component.selectedComponent = slabsCustomComponentsMock[0];
    component.changeComponent();
    expect(dialogRefMock.close).toHaveBeenCalled();
  });

  it('should close dialog', () => {
    spyOn(dialogRefMock, 'close');
    component.closeDialog();
    expect(dialogRefMock.close).toHaveBeenCalled();
  });
});
