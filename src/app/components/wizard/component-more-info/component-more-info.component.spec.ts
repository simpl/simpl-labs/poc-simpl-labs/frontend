import {
  ComponentFixture,
  fakeAsync,
  TestBed,
  tick,
} from '@angular/core/testing';
import { ActivatedRoute, ParamMap } from '@angular/router';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { SimpleChange } from '@angular/core';
import { MatTableDataSource } from '@angular/material/table';
import { of } from 'rxjs';

import { ComponentMoreInfoComponent } from './component-more-info.component';
import { dataSpacesMock } from '../../../../mocks/data-space-mock';
import { CategoryType } from '../../../models/enums/data-space/category-type.enum';
import { SlabsCustomComponentService } from '../../../services/slabs-custom-component.service';
import { slabsCustomComponentsMock } from '../../../../mocks/slabs-custom-component-mock';
import { ComponentModel } from '../../../models/interfaces/data-space/component.interface';
import { AppService } from '../../../services/app.service';
import { envUrl } from '../../../../tests/env-url-mock';

const mockComponent: ComponentModel = {
  ...dataSpacesMock[0].nodeList[0].categoryList[0].componentList[0],
};

const stubParamMap: ParamMap = {
  has: (name: string) => !!name,
  get: (name: string) => name,
  getAll: (name: string) => [name],
  keys: ['a', 'b'],
};

class MockActivatedRoute {
  paramMap = of(stubParamMap);
}

describe('ComponentMoreInfoComponent', () => {
  let component: ComponentMoreInfoComponent;
  let fixture: ComponentFixture<ComponentMoreInfoComponent>;
  let customComponentService: SlabsCustomComponentService;
  let appServiceSpy: jasmine.SpyObj<AppService>;

  beforeEach(async () => {
    appServiceSpy = jasmine.createSpyObj('AppService', ['load']);

    await TestBed.configureTestingModule({
      imports: [ComponentMoreInfoComponent, HttpClientTestingModule],
      providers: [
        SlabsCustomComponentService,
        { provide: ActivatedRoute, useClass: MockActivatedRoute },
        { provide: AppService, useValue: appServiceSpy },
      ],
    }).compileComponents();

    fixture = TestBed.createComponent(ComponentMoreInfoComponent);
    component = fixture.componentInstance;

    component.currentSlide = 1;
    component.dotHelper = [];
    component.tableColumns = ['componentProperty', 'componentValue'];
    component.tableDataSource = new MatTableDataSource<{
      componentProperty: string;
      componentValue: string;
    }>();

    customComponentService = TestBed.inject(SlabsCustomComponentService);
    customComponentService.customComponents = slabsCustomComponentsMock;

    Object.defineProperty(appServiceSpy, 'appUrls', {
      get: () => ({
        apiUrl: 'http://api-mock.org',
        simplUrl: '"http://simpl-mock.org/',
      }),
    });

    fixture.detectChanges();

    spyOnProperty(appServiceSpy, 'appUrls', 'get').and.returnValue(envUrl);
    spyOn(component, 'mapComponent2TableData');
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should call #mapComponent2TableData()', () => {
    const componentInfo = {
      ...mockComponent,
      categoryType: CategoryType.LOGGING,
    };
    const change = new SimpleChange(undefined, componentInfo, true);
    component.ngOnChanges({ componentInfo: change });

    expect(component.mapComponent2TableData).toHaveBeenCalledWith(
      componentInfo,
    );
  });

  it('should call #mapComponent2TableData()', fakeAsync(() => {
    spyOn(component, 'initiateKeenSlider');
    const comp = [...dataSpacesMock[1].nodeList][0].categoryList[0]
      .componentList[0];
    const componentInfo = { ...comp, categoryType: CategoryType.LOGGING };
    const change = new SimpleChange(undefined, componentInfo, true);
    component.ngOnChanges({ componentInfo: change });

    tick(50);

    expect(component.initiateKeenSlider).toHaveBeenCalled();
  }));

  it('should close the Drawer', () => {
    spyOn(component.closeDrawer, 'emit');
    component.onCloseDrawerClick();
    expect(component.closeDrawer.emit).toHaveBeenCalled();
  });

  it('should return CustomComponent name', () => {
    const name = customComponentService.customComponents.find(
      (cc) => cc.id === mockComponent.customComponentId,
    )?.name!;
    const _name = component.getCustomComponentName(mockComponent);
    expect(_name).toBe(name);
  });
});
