import {
  Component,
  ElementRef,
  EventEmitter,
  Input,
  OnChanges,
  OnDestroy,
  Output,
  SimpleChanges,
  ViewChild,
} from '@angular/core';
import { MatTableDataSource, MatTableModule } from '@angular/material/table';
import { CommonModule } from '@angular/common';
import { RouterLink } from '@angular/router';
import { MatIconModule } from '@angular/material/icon';
import KeenSlider, { KeenSliderInstance } from 'keen-slider';

import { ComponentModel } from '../../../models/interfaces/data-space/component.interface';
import { TitlecaseNodeName } from '../../../pipes/titlecase-node-name.pipe';
import { SlabsCustomComponentService } from '../../../services/slabs-custom-component.service';
import { ComponentInfo } from '../../../models/interfaces/data-space/component-info.interface';
import { AppService } from '../../../services/app.service';

@Component({
  selector: 'app-component-more-info',
  standalone: true,
  imports: [
    CommonModule,
    RouterLink,
    MatTableModule,
    MatIconModule,
    TitlecaseNodeName,
  ],
  templateUrl: './component-more-info.component.html',
  styleUrl: './component-more-info.component.scss',
})
export class ComponentMoreInfoComponent implements OnChanges, OnDestroy {
  @Input() componentInfo!: ComponentInfo;
  @Output() closeDrawer = new EventEmitter<void>();
  @ViewChild('sliderRef') sliderRef!: ElementRef<HTMLElement>;

  simplUrl = '';
  currentSlide: number = 1;
  dotHelper: Array<number> = [];
  slider: KeenSliderInstance = null!;

  tableDataSource = new MatTableDataSource<{
    componentProperty: string;
    componentValue: string;
  }>();
  tableColumns: string[] = ['componentProperty', 'componentValue'];

  constructor(
    private readonly slabsCustomComponentService: SlabsCustomComponentService,
    private readonly appService: AppService,
  ) {
    this.simplUrl = this.appService.appUrls?.simplUrl;
  }

  ngOnChanges(changes: SimpleChanges): void {
    if (changes.hasOwnProperty('componentInfo')) {
      const { currentValue } = changes['componentInfo'];
      this.mapComponent2TableData(currentValue);
      setTimeout(() => this.initiateKeenSlider(), 50);
    }
  }

  ngOnDestroy(): void {
    if (this.slider) this.slider.destroy();
  }

  mapComponent2TableData(comp: ComponentModel) {
    const props = ['author', 'lastUpdate', 'repository'];
    /**
     * The following Values are temporarly "dummy", as they are
     * currently not present in ComponentModel
     */
    const values = [
      comp?.customImage ? 'My Tenant' : 'Simpl Open',
      '24/4/2024',
      comp?.customImage ? 'github.my-tenant.com' : 'github.simpl.com',
    ];

    this.tableDataSource.data = props.map((prop, i) => {
      return {
        componentProperty: prop === 'lastUpdate' ? 'Last update' : prop,
        componentValue: values[i],
      };
    });
  }

  initiateKeenSlider() {
    this.slider = new KeenSlider(this.sliderRef.nativeElement, {
      initial: this.currentSlide,
      slideChanged: (s) => (this.currentSlide = s.track.details.rel),
      slides: {
        perView: 2,
      },
    });
    this.dotHelper = [...Array(this.slider.track.details.slides.length).keys()];
  }

  onCloseDrawerClick() {
    this.closeDrawer.emit();
  }

  getCustomComponentName(comp: ComponentModel): string {
    return this.slabsCustomComponentService.customComponents.find(
      (cc) => cc.id === comp.customComponentId,
    )?.name!;
  }
}
