import {
  ComponentFixture,
  ComponentFixtureAutoDetect,
  TestBed,
} from '@angular/core/testing';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { Router } from '@angular/router';
import { Observable, of, Subject } from 'rxjs';

import { DataSpaceComponent } from './data-space.component';
import { DataSpaceService } from '../../services/data-space.service';
import { dataSpacesMock } from '../../../mocks/data-space-mock';
import { DataSpaceModel } from '../../models/interfaces/data-space/data-space.interface';
import { MockRouter } from '../../../tests/helpers/mock-router';
import { DataSpaceStatus } from '../../models/enums/data-space/data-space-status.enum';

let mockDataSpaceService: Partial<DataSpaceService> = {
  nDataSpaces: 1,
  dataSpacesSubject$: new Subject<DataSpaceModel[]>(),
  postDataSpaceSubject$: new Subject<DataSpaceModel>(),

  getDataSpaces(): Observable<DataSpaceModel[]> {
    return of(dataSpacesMock);
  },
  getDataSpacesNumber(): number {
    return this.nDataSpaces!;
  },
  setDataSpacesNumber(n: number) {
    this.nDataSpaces = n;
  },
};

describe('DataSpaceComponent', () => {
  let component: DataSpaceComponent;
  let fixture: ComponentFixture<DataSpaceComponent>;
  let h3: HTMLElement;
  let host: any;
  let optionMenuBtn: HTMLElement;
  let dataspacesNumberElm: HTMLElement;
  let mockRouter = new MockRouter();

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports: [
        DataSpaceComponent,
        HttpClientTestingModule,
        BrowserAnimationsModule,
      ],
      providers: [
        { provide: DataSpaceService, useValue: mockDataSpaceService },
        { provide: ComponentFixtureAutoDetect, useValue: true },
        { provide: Router, useValue: mockRouter },
      ],
    }).compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(DataSpaceComponent);
    fixture.detectChanges();

    TestBed.inject(DataSpaceService);

    spyOn(mockDataSpaceService, 'getDataSpaces' as any).and.callThrough();

    component = fixture.debugElement.componentInstance;
    h3 = fixture.nativeElement.querySelector('h3');
    host = fixture.nativeElement;
    optionMenuBtn = fixture.nativeElement.querySelector('.simplut-menu-btn');
    dataspacesNumberElm =
      fixture.nativeElement.querySelector('.dataspaces-number');
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should display the title "Your Data Spaces"', () => {
    component.isConformanceTest = false;
    expect(h3.textContent).toBe('Your Data Spaces');
  });

  it('should display no title when isConformanceTest', () => {
    component.isConformanceTest = true;
    fixture.detectChanges();
    expect(h3.textContent).toBe('');
  });

  it('no dataspacesNumberElm content in the HOST after createComponent()', () => {
    expect(dataspacesNumberElm.textContent).toEqual('  ');
  });

  it('should display dataSpaces Number', async () => {
    component.isConformanceTest = false;
    await fixture.whenStable();
    expect(dataspacesNumberElm.textContent).toContain(
      component.storedDataSpaces.length,
    );
  });

  it('should not have the menu open', async () => {
    const menu = host.parent?.nativeElement.querySelector(
      '.mat-mdc-menu-panel',
    );
    expect(menu).toBeUndefined();
  });

  it('open the menu when clicking on the optionMenuBtn', async () => {
    component.filteredDataSpaces = dataSpacesMock.slice(0, 2);
    fixture.detectChanges();
    optionMenuBtn =
      fixture.debugElement.nativeElement.querySelector('.simplut-menu-btn');
    optionMenuBtn.dispatchEvent(new Event('click'));
    const menu = host.parentNode?.querySelector('.mat-mdc-menu-panel');
    expect(menu).toBeTruthy();
  });

  it('should call #openWarningDialog()', () => {
    spyOn(component, 'openWarningDialog');
    component.onCreateDataSpaceClick();
    expect(component.openWarningDialog).toHaveBeenCalled();
  });

  it('should call #goToDataSpaceCreation()', () => {
    component.limitDataSpaces = 2;
    spyOn(component, 'goToDataSpaceCreation');
    component.onCreateDataSpaceClick();
    expect(component.goToDataSpaceCreation).toHaveBeenCalled();
  });

  it('should call navigateByUrl() when #goToDataSpaceCreation()', () => {
    spyOn(mockRouter, 'navigateByUrl');
    component.goToDataSpaceCreation();
    expect(mockRouter.navigateByUrl).toHaveBeenCalled();
  });

  it('should not call navigateByUrl() when (#viewDataSpace() && status != RUNNING )', () => {
    spyOn(mockRouter, 'navigateByUrl');
    component.viewDataSpace({
      ...dataSpacesMock[0],
      status: DataSpaceStatus.REQUESTED,
    });
    expect(mockRouter.navigateByUrl).not.toHaveBeenCalled();
  });

  it('should not call navigateByUrl() when (#viewDataSpace() && status == RUNNING )', () => {
    spyOn(mockRouter, 'navigateByUrl');
    component.viewDataSpace({
      ...dataSpacesMock[0],
      status: DataSpaceStatus.RUNNING,
    });
    expect(mockRouter.navigateByUrl).toHaveBeenCalled();
  });

  it('should call navigateByUrl() when #viewDataSpace()', () => {
    spyOn(component, 'openWarningDialog');
    component.deleteDataSpace(dataSpacesMock[0]);
    expect(component.openWarningDialog).toHaveBeenCalled();
  });

  it('should set #storedDataSpaces value (p.1)', () => {
    component.storedDataSpaces = dataSpacesMock.slice(0, 2);
    component.storeAndFilter(dataSpacesMock);
    expect(component.storedDataSpaces).toEqual(dataSpacesMock);
  });

  it('should set #storedDataSpaces value (p.2)', () => {
    component.storedDataSpaces = dataSpacesMock.slice(0, 3);
    component.storeAndFilter(dataSpacesMock.slice(0, 3));
    expect(component.storedDataSpaces).toEqual(dataSpacesMock.slice(0, 3));
  });

  it('should add dataSpace to #dataSpaces', () => {
    component.dataSpaces = dataSpacesMock.slice(0, 2);
    const LEN = component.dataSpaces.length;
    mockDataSpaceService.postDataSpaceSubject$?.next(dataSpacesMock[2]);
    expect(component.dataSpaces.length).toBe(LEN + 1);
  });
});
