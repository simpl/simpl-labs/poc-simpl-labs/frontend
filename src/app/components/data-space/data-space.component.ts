import { Component, Input, OnDestroy, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { CommonModule } from '@angular/common';
import { Observable, Subscription, map, tap, timer } from 'rxjs';
import { MatButtonModule } from '@angular/material/button';
import { MatIconModule } from '@angular/material/icon';
import { MatCardModule } from '@angular/material/card';
import { MatGridListModule } from '@angular/material/grid-list';
import { MatChipsModule } from '@angular/material/chips';
import { MatDialog } from '@angular/material/dialog';
import { MatMenuModule } from '@angular/material/menu';
import { BreakpointObserver } from '@angular/cdk/layout';

import { DataSpaceService } from '../../services/data-space.service';
import { DataSpaceStatus } from '../../models/enums/data-space/data-space-status.enum';
import { DataSpaceModel } from '../../models/interfaces/data-space/data-space.interface';
import { NodeType } from '../../models/enums/data-space/node-type.enum';
import { LoadingService } from '../../services/loading.service';
import { DialogService } from '../../services/dialog.service';
import mapDataSpaceStatus2Chip from '../../helpers/data-space-chip-colors';
import { SearchFiltersFormComponent } from '../_shared/search-filters-form/search-filters-form.component';
import { WarningDialogData } from '../../models/interfaces/warning-dialog-data.interface';

@Component({
  selector: 'app-data-space',
  standalone: true,
  imports: [
    CommonModule,
    MatButtonModule,
    MatIconModule,
    MatCardModule,
    MatGridListModule,
    MatChipsModule,
    MatMenuModule,
    SearchFiltersFormComponent,
  ],
  templateUrl: './data-space.component.html',
  styleUrl: './data-space.component.scss',
})
export class DataSpaceComponent implements OnInit, OnDestroy {
  @Input() isConformanceTest = false;
  @Input() customComponentDataSpaces$!: Observable<DataSpaceModel[]>;

  cols$ = new Observable<number>();

  limitDataSpaces!: number;
  storedDataSpaces: DataSpaceModel[] = [];
  filteredDataSpaces!: DataSpaceModel[];

  statuses = DataSpaceStatus;
  dataSpaces: DataSpaceModel[] = [];
  nComponents: number[] = [];
  status2ChipColor = mapDataSpaceStatus2Chip;

  getDataSpacesSub = new Subscription();
  postDataSpaceSub = new Subscription();
  deleteDataSpaceSub = new Subscription();
  routerEventsSub = new Subscription();
  timerSubscription = new Subscription();
  componentDataSpacesSub = new Subscription();

  constructor(
    private readonly dataSpaceService: DataSpaceService,
    private readonly router: Router,
    private readonly dialogService: DialogService,
    private readonly _breakpointObserver: BreakpointObserver,
    private readonly _loadingService: LoadingService,
    public dialog: MatDialog,
  ) {
    this.limitDataSpaces = this.dataSpaceService.dataSpacesLimit;

    this.cols$ = this._breakpointObserver
      .observe([`(max-width: ${1430}px)`, `(max-width: ${2080}px)`])
      .pipe(
        map((state) => {
          if (state.breakpoints[`(max-width: ${1430}px)`]) return 1;
          if (state.breakpoints[`(max-width: ${2080}px)`]) return 2;
          return 3;
        }),
      );
  }

  ngOnInit(): void {
    if (!this.customComponentDataSpaces$)
      this.timerSubscription = timer(0, this.dataSpaceService.timerInterval)
        .pipe(
          tap((_timer) => this._loadingService.updateTimer(_timer)),
          map((_timer) => (this.getDataSpacesSub = this.getDataSpaces())),
        )
        .subscribe();
    else
      this.componentDataSpacesSub = this.customComponentDataSpaces$.subscribe(
        (dataSpaces) => this.storeAndFilter(dataSpaces),
      );

    this.postDataSpaceSub = this.dataSpaceService.postDataSpaceSubject$
      .asObservable()
      .subscribe((dataSpace) => this.dataSpaces.push(dataSpace));
  }

  ngOnDestroy(): void {
    this.timerSubscription.unsubscribe();
    this.getDataSpacesSub.unsubscribe();
    this.postDataSpaceSub.unsubscribe();
    this.deleteDataSpaceSub.unsubscribe();
    this.routerEventsSub.unsubscribe();
    this.componentDataSpacesSub.unsubscribe();
  }

  getDataSpaces(): Subscription {
    return this.dataSpaceService.getDataSpaces().subscribe((dataSpaces) => {
      this.dataSpaceService.dataSpacesSubject$.next(dataSpaces);
      this.storeAndFilter(dataSpaces);
      this._getComponentsNumber();
    });
  }

  storeAndFilter(dataSpaces: DataSpaceModel[]) {
    this.dataSpaces = dataSpaces.map((ds) => {
      ds.nodeList = ds.nodeList.map((n) => {
        n.name =
          n.name === `${NodeType.GOVERNANCE}`
            ? `${NodeType.GOVERNANCE}_AUTHORITY`
            : n.name;
        return n;
      });

      return ds;
    });

    if (this.storedDataSpaces.length === dataSpaces.length) {
      this.storedDataSpaces = this.storedDataSpaces.map((storedDS) => {
        const _ds = dataSpaces.find((ds) => ds.id === storedDS.id);
        storedDS.status = _ds?.status!;
        return storedDS;
      });
    } else {
      this.storedDataSpaces = dataSpaces;
      this.filteredDataSpaces = dataSpaces;
    }

    this.filteredDataSpaces = this.filteredDataSpaces ?? this.storedDataSpaces;

    if (this.dataSpaceService.getDataSpacesNumber() !== dataSpaces.length)
      this.dataSpaceService.setDataSpacesNumber(dataSpaces.length);
  }

  onCreateDataSpaceClick() {
    this.limitDataSpaces && this.dataSpaces.length < this.limitDataSpaces
      ? this.goToDataSpaceCreation()
      : this.openWarningDialog({
          title: 'Data Space creation disabled',
          content: `Users are allowed to create up to ${this.limitDataSpaces} Data Spaces`,
        });
  }

  goToDataSpaceCreation() {
    this.router.navigateByUrl('data-spaces/create-data-space');
  }

  openWarningDialog<T>(data: WarningDialogData<T>) {
    let dialogRef = this.dialogService.openWarningDialog<T>(data);

    dialogRef.afterClosed().subscribe((data: WarningDialogData<T>) => {
      if (data.confirm && typeof data.callerData === 'number')
        this.deleteDataSpaceSub = this.dataSpaceService
          .deleteDataSpace(data.callerData)
          .subscribe((dataSpace) => {
            const _dataSpace = this.storedDataSpaces.find(
              (ds) => ds.id === dataSpace.id,
            );
            if (_dataSpace)
              _dataSpace.status = DataSpaceStatus.REQUESTED_DELETION;
          });
    });
  }

  private _getComponentsNumber() {
    this.nComponents = this.dataSpaces.map((ds) => {
      let n = 0;
      ds.nodeList.forEach((node) =>
        node.categoryList.forEach(
          (category) => (n += category.componentList.length),
        ),
      );
      return n;
    });
  }

  viewDataSpace(dataSpace: DataSpaceModel) {
    return dataSpace.status !== DataSpaceStatus.RUNNING
      ? null
      : this.router.navigateByUrl(
          `data-spaces/data-space-view/${dataSpace.id}`,
        );
  }

  deleteDataSpace(dataSpace: DataSpaceModel) {
    this.openWarningDialog<number>({
      title: `Delete Data Space`,
      content: `Do you really want to delete the Data Space`,
      boldContent: dataSpace.name,
      confirm: true,
      callerData: dataSpace.id,
    });
  }

  onFilterDataSpaces(event: DataSpaceModel[]) {
    this.filteredDataSpaces = event;
  }
}
