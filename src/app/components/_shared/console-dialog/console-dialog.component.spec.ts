import { ComponentFixture, TestBed } from '@angular/core/testing';
import { MatDialogRef } from '@angular/material/dialog';
import { HttpClientTestingModule } from '@angular/common/http/testing';

import { ConsoleDialogComponent } from './console-dialog.component';

const dialogRefMock = {
  close: () => {},
};

describe('ConsoleDialogComponent', () => {
  let component: ConsoleDialogComponent;
  let fixture: ComponentFixture<ConsoleDialogComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports: [HttpClientTestingModule, ConsoleDialogComponent],
      providers: [{ provide: MatDialogRef, useValue: dialogRefMock }],
    }).compileComponents();

    fixture = TestBed.createComponent(ConsoleDialogComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should close dialog', () => {
    spyOn(dialogRefMock, 'close');
    component.goBack();
    expect(dialogRefMock.close).toHaveBeenCalled();
  });
});
