import { FlatTreeControl } from '@angular/cdk/tree';
import {
  Component,
  Input,
  OnChanges,
  OnDestroy,
  OnInit,
  SimpleChanges,
  ViewChild,
} from '@angular/core';
import { CommonModule } from '@angular/common';
import {
  MatTreeFlattener,
  MatTreeFlatDataSource,
  MatTreeModule,
} from '@angular/material/tree';
import { FormsModule } from '@angular/forms';
import { Subscription } from 'rxjs';
import { MatButtonModule } from '@angular/material/button';
import { MatIconModule } from '@angular/material/icon';
import { MatToolbarModule } from '@angular/material/toolbar';
import { MatMenuModule } from '@angular/material/menu';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatInputModule } from '@angular/material/input';

import { NodeType } from '../../../models/enums/data-space/node-type.enum';
import {
  SLabsNode,
  SLabsFlatNode,
} from '../../../models/interfaces/tree-node.interface';
import { NodeModel } from '../../../models/interfaces/data-space/node.interface';
import { GraphChartDatum } from '../../../models/interfaces/graph-chart-datum.interface';
import { TreeMenuService } from '../../../services/tree-menu.service';
import { TitlecaseNodeName } from '../../../pipes/titlecase-node-name.pipe';
import { chartNode2SlabsFlatNode } from '../../../helpers/chart-node-2-slabs-flat-node.map';
import { OptionsMenuComponent } from '../options-menu/options-menu.component';

@Component({
  selector: 'app-context-tree-menu',
  standalone: true,
  imports: [
    CommonModule,
    FormsModule,
    MatButtonModule,
    MatIconModule,
    MatTreeModule,
    MatMenuModule,
    MatFormFieldModule,
    MatInputModule,
    MatToolbarModule,
    TitlecaseNodeName,
    OptionsMenuComponent,
  ],
  templateUrl: './context-tree-menu.component.html',
  styleUrl: './context-tree-menu.component.scss',
})
export class ContextTreeMenuComponent implements OnInit, OnChanges, OnDestroy {
  @Input() isView = false;
  @Input() isMonitoring = false;
  @Input() nodeList!: NodeModel[];

  @ViewChild(OptionsMenuComponent, {
    read: OptionsMenuComponent,
    static: true,
  })
  optionsMenuComponent!: OptionsMenuComponent;

  selectedNode!: SLabsFlatNode;

  filterValue = '';

  graphChartDatum: GraphChartDatum = {
    name: '',
    x: 0,
    y: 0,
    id: null!,
  };

  _transformer = (node: SLabsNode, level: number) => {
    return {
      expandable: !!node.list && node.list.length > 0,
      name: node.name,
      level: level,
      id: node.id,
      type: node.type,
    };
  };

  treeControl = new FlatTreeControl<SLabsFlatNode>(
    (node) => node.level,
    (node) => node.expandable,
  );

  treeFlattener = new MatTreeFlattener(
    this._transformer,
    (node) => node.level,
    (node) => node.expandable,
    (node) => node.list,
  );

  treeFlatDataSource = new MatTreeFlatDataSource(
    this.treeControl,
    this.treeFlattener,
  );

  graphChartNodeSubscription = new Subscription();

  constructor(private readonly treeMenuService: TreeMenuService) {}

  ngOnChanges(changes: SimpleChanges): void {
    if (changes.hasOwnProperty('nodeList')) {
      const { currentValue } = changes['nodeList'];
      if (currentValue) {
        this.mapDataSpace2TreeData(currentValue);

        const breadcrumbNode: SLabsFlatNode | undefined =
          this.treeControl.dataNodes.find((dn) => dn.expandable && dn.expanded);
        if (breadcrumbNode)
          this.treeMenuService.updateBreadcrumbs(
            breadcrumbNode,
            this.isMonitoring,
          );
      }
    }
  }

  ngOnInit(): void {
    this.graphChartNodeSubscription =
      this.treeMenuService.graphChartNodeSubject$
        .asObservable()
        .subscribe((graphChartNode) => this.updateFromChart(graphChartNode));
  }

  ngOnDestroy(): void {
    this.graphChartNodeSubscription.unsubscribe();
  }

  filterParentNode(node: SLabsFlatNode): boolean {
    return !this.filterValue ||
      node.name.toLowerCase().indexOf(this.filterValue?.toLowerCase()) !== -1
      ? false
      : !this.treeControl
          .getDescendants(node)
          .some(
            (descendantNode) =>
              descendantNode.name
                .toLowerCase()
                .indexOf(this.filterValue?.toLowerCase()) !== -1,
          );
  }

  filterLeafNode(node: SLabsFlatNode): boolean {
    return !this.filterValue
      ? false
      : node.name.toLowerCase().indexOf(this.filterValue?.toLowerCase()) === -1;
  }

  updateFromChart(graphChartNode: GraphChartDatum) {
    this.isMonitoring &&
      (this.selectedNode = chartNode2SlabsFlatNode(graphChartNode));

    graphChartNode.level! < 0 ||
    (graphChartNode.level === 0 && graphChartNode.collapsed)
      ? this.treeControl.collapseAll()
      : this.treeControl.dataNodes.some((dn) => {
          let found =
            dn.name === (graphChartNode.ancestorName ?? graphChartNode.name);
          if (found)
            if (graphChartNode.ancestorName)
              this.treeControl.getDescendants(dn).forEach((descendant) => {
                if (descendant.name === graphChartNode.name)
                  graphChartNode.collapsed
                    ? this.treeControl.collapse(descendant)
                    : this.treeControl.expand(descendant);
                else this.treeControl.collapseDescendants(descendant);
              });
            else if (graphChartNode.collapsed) this.treeControl.collapse(dn);
            else {
              this.treeControl.expand(dn);
              this.treeControl.dataNodes.some((dn) => {
                let found =
                  dn.level === graphChartNode.level! + 1 &&
                  this.treeControl.isExpanded(dn);
                if (found) this.treeControl.collapse(dn);
                return found;
              });
            }
          return found;
        });
  }

  mapDataSpace2TreeData(nodeList: NodeModel[]): void {
    this.treeFlatDataSource.data = [];

    this.treeFlatDataSource.data = nodeList.map((node) => {
      const dsNode: SLabsNode = {
        name: node.name,
        type: node.type,
        id: node.id!,
      };
      dsNode.list = node.categoryList.map((category) => {
        const dsCategory: SLabsNode = {
          name: category.type,
          type: category.type as unknown as NodeType,
          id: category.id!,
        };
        dsCategory.list = category.componentList.map((component) => {
          return {
            name: component.type as string,
            type: component.type as unknown as NodeType,
            id: component.id!,
          };
        });
        return dsCategory;
      });
      return dsNode;
    });
  }

  onTreeMenuNodeClick(node: SLabsFlatNode) {
    if (this.treeControl.isExpanded(node))
      this.treeControl.dataNodes
        .filter((dn) => dn.level === node.level && dn.name !== node.name)
        .forEach((dn) => this.treeControl.collapseDescendants(dn));
    else this.treeControl.collapseDescendants(node);

    node.expanded = this.treeControl.isExpanded(node);
    this.treeMenuService.treeMenuNodeSubject$.next(node);

    this.treeMenuService.updateBreadcrumbs(node, this.isMonitoring);

    this.selectedNode = node;
  }

  isSelected(node: SLabsFlatNode): boolean {
    return node.id === this.selectedNode.id && node.level === 2;
  }

  hasChild = (_: number, node: SLabsFlatNode) => node.expandable;

  showNodeMenu(node: SLabsFlatNode): boolean {
    return !this.isView && node.level === 0;
  }

  openOptionsMenu(event: MouseEvent, node: SLabsFlatNode) {
    this.graphChartDatum = {
      name: node.name,
      id: node.id?.toString(),
      type: node.type as NodeType,
      x: event.pageX,
      y: event.pageY - 140,
    };

    this.optionsMenuComponent.openMenu();
  }
}
