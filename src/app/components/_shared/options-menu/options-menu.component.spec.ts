import { ComponentFixture, TestBed } from '@angular/core/testing';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

import { OptionsMenuComponent } from './options-menu.component';
import { GraphChartDatum } from '../../../models/interfaces/graph-chart-datum.interface';
import { dataSpacesMock } from '../../../../mocks/data-space-mock';
import { NodeModel } from '../../../models/interfaces/data-space/node.interface';
import { createMouseEvent } from '../../../../tests/helpers/create-mouse-event.function';
import { TreeMenuService } from '../../../services/tree-menu.service';

const mockMenuData: GraphChartDatum = {
  name: '',
  x: 0,
  y: 0,
  id: null!,
};

const mockNodeList: NodeModel[] = dataSpacesMock[1].nodeList;

describe('OptionsMenuComponent', () => {
  let component: OptionsMenuComponent;
  let fixture: ComponentFixture<OptionsMenuComponent>;
  let treeMenuServiceSpy: jasmine.SpyObj<TreeMenuService>;
  let host: any;

  beforeEach(async () => {
    treeMenuServiceSpy = jasmine.createSpyObj('TreeMenuService', [
      'renameNode',
      'duplicateNode',
      'deleteNode',
    ]);

    await TestBed.configureTestingModule({
      imports: [
        OptionsMenuComponent,
        HttpClientTestingModule,
        BrowserAnimationsModule,
      ],
      providers: [{ provide: TreeMenuService, useValue: treeMenuServiceSpy }],
    }).compileComponents();

    fixture = TestBed.createComponent(OptionsMenuComponent);
    component = fixture.componentInstance;
    host = fixture.nativeElement;
    component.newNodeName = '';
    component.node = mockMenuData;
    component.nodeList = mockNodeList;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should not have the menu open', () => {
    const menu = host.parent?.nativeElement.querySelector(
      '.mat-mdc-menu-panel',
    );
    expect(menu).toBeUndefined();
  });

  it('should open the menu when triggered by #openMenu()', () => {
    component.openMenu();

    const menu = host.parentNode?.querySelector('.mat-mdc-menu-panel');
    expect(menu).toBeTruthy();
  });

  it('should close the menu when triggered by #optsMenuTrigger', () => {
    component.optsMenuTrigger.closeMenu();

    const menu = host.parentNode?.querySelector('.mat-mdc-menu-panel');
    expect(menu).toBeNull();
  });

  it('should call #onMenuItemClick() when clicked on rename menu option', () => {
    spyOn(component, 'onMenuItemClick');

    component.inputFieldShown = false;

    component.openMenu();

    const renameBtn = host.parentNode?.querySelector('.simplut-rename-btn');
    const renameInput = host.parentNode?.querySelector('.simplut-rename-input');
    expect(renameBtn).toBeTruthy();
    expect(renameInput).toBeNull();

    renameBtn.click();
    expect(component.onMenuItemClick).toHaveBeenCalled();
  });

  it('should call #onMenuItemClick() when clicked on rename menu option', () => {
    spyOn(component, 'onMenuItemClick');

    component.inputFieldShown = true;
    fixture.detectChanges();

    component.openMenu();

    const renameBtn = host.parentNode?.querySelector('.simplut-rename-btn');
    const renameInput = host.parentNode?.querySelector('.simplut-rename-input');
    expect(renameBtn).toBeNull();
    expect(renameInput).toBeTruthy();

    renameInput.click();
    expect(component.onMenuItemClick).toHaveBeenCalled();
  });

  it('should set #inputFieldShown to true', () => {
    component.inputFieldShown = false;
    const _event = createMouseEvent();
    fixture.detectChanges();
    component.onMenuItemClick(_event);

    expect(component.inputFieldShown).toBe(true);
  });

  it('should update the DataSpace upon node deletion', () => {
    const node: GraphChartDatum = {
      name: mockNodeList[1].name,
      type: mockNodeList[1].type,
      x: 123,
      y: 456,
      id: mockNodeList[1].id?.toString()!,
    };
    component.deleteNode(node);

    expect(treeMenuServiceSpy.deleteNode).toHaveBeenCalledWith(
      node,
      component.nodeList,
    );
  });

  it('should update the DataSpace upon node duplication', () => {
    const node: GraphChartDatum = {
      name: mockNodeList[1].name,
      type: mockNodeList[1].type,
      x: 123,
      y: 456,
      id: mockNodeList[1].id?.toString()!,
    };
    component.duplicateNode(node);

    expect(treeMenuServiceSpy.duplicateNode).toHaveBeenCalledWith(
      node,
      component.nodeList,
    );
  });

  it('should update the DataSpace upon node renaming', () => {
    component.newNodeName = 'foo';
    const node: GraphChartDatum = {
      name: mockNodeList[0].name,
      type: mockNodeList[0].type,
      x: 123,
      y: 456,
      id: mockNodeList[0].id?.toString()!,
    };
    component.renameNode(node);

    const _nodeList = [...mockNodeList];
    _nodeList[0].name = 'foo';

    expect(treeMenuServiceSpy.renameNode).toHaveBeenCalledWith(
      node,
      _nodeList,
      _nodeList[0].name,
    );
  });
});
