import {
  Component,
  Input,
  OnChanges,
  OnDestroy,
  SimpleChanges,
  ViewChild,
} from '@angular/core';
import { FormsModule } from '@angular/forms';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatIconModule } from '@angular/material/icon';
import { MatMenu, MatMenuModule, MatMenuTrigger } from '@angular/material/menu';
import { MatInputModule } from '@angular/material/input';
import { MatButtonModule } from '@angular/material/button';

import { NodeType } from '../../../models/enums/data-space/node-type.enum';
import { GraphChartDatum } from '../../../models/interfaces/graph-chart-datum.interface';
import { NodeModel } from '../../../models/interfaces/data-space/node.interface';
import { TreeMenuService } from '../../../services/tree-menu.service';
import { SLabsFlatNode } from '../../../models/interfaces/tree-node.interface';

@Component({
  selector: 'app-options-menu',
  standalone: true,
  imports: [
    FormsModule,
    MatMenuModule,
    MatIconModule,
    MatButtonModule,
    MatFormFieldModule,
    MatInputModule,
  ],
  templateUrl: './options-menu.component.html',
})
export class OptionsMenuComponent implements OnChanges, OnDestroy {
  @Input() node!: GraphChartDatum;
  @Input() nodeList: NodeModel[] = [];

  @ViewChild('optsMenuTrigger', { read: MatMenuTrigger, static: true })
  optsMenuTrigger!: MatMenuTrigger;

  @ViewChild('optionsMenu', { read: MatMenu, static: true })
  optionsMenu!: MatMenu;

  inputFieldShown = false;
  newNodeName = '';
  x!: number;
  y!: number;

  constructor(private readonly treeMenuService: TreeMenuService) {}

  ngOnChanges(changes: SimpleChanges): void {
    if (changes.hasOwnProperty('node')) {
      this.x = changes['node'].currentValue.x;
      this.y = changes['node'].currentValue.y;
    }
  }

  ngOnDestroy(): void {
    this.optsMenuTrigger.closeMenu();
  }

  openMenu() {
    this.optsMenuTrigger.openMenu();
  }

  resetNodeMenu() {
    this.newNodeName = '';
    this.inputFieldShown = false;
  }

  onMenuItemClick(event: { stopPropagation: () => void }) {
    event.stopPropagation();
    this.inputFieldShown = true;
  }

  renameNode(node: GraphChartDatum) {
    this.treeMenuService.renameNode(node, this.nodeList, this.newNodeName);
    this.optsMenuTrigger.closeMenu();
    this.resetNodeMenu();
  }

  duplicateNode(node: GraphChartDatum) {
    this.treeMenuService.duplicateNode(node, this.nodeList);
  }

  deleteNode(node: GraphChartDatum) {
    this.treeMenuService.deleteNode(node, this.nodeList);
  }

  isGovernanceNode(node: GraphChartDatum): boolean {
    const idx = this.nodeList?.findIndex((n) => n.name === node.name);
    return idx >= 0 && this.nodeList[idx].type === NodeType.GOVERNANCE;
  }

  isDeletable(node: SLabsFlatNode | GraphChartDatum): boolean {
    if (
      node.type === NodeType.APPLICATION_PROVIDER ||
      node.type === NodeType.INFRASTRUCTURE_PROVIDER
    )
      return true;
    else if (
      node.type === NodeType.CONSUMER ||
      node.type === NodeType.DATA_PROVIDER
    )
      return this.nodeList.findIndex((n) => n.name === node.name) > 2;
    return false;
  }
}
