import * as echarts from 'echarts/core';
import { GridComponentOption } from 'echarts';
import { LineSeriesOption } from 'echarts/charts';

export type TenantConsumptionOption = echarts.ComposeOption<
  LineSeriesOption | GridComponentOption
>;

export const tenantConsumptionOption: TenantConsumptionOption = {
  xAxis: {
    show: false,
    type: 'value',
    min: undefined,
    max: undefined,
  },
  yAxis: {
    show: false,
    type: 'value',
    min: 0,
    max: 1.5,
  },
  grid: {
    left: 0,
    right: 0,
    bottom: 0,
    top: 0,
    height: 'auto',
  },
  series: [
    {
      animation: true,
      animationDuration: 5,
      animationDurationUpdate: 5,
      animationEasing: 'backInOut',
      animationEasingUpdate: 'circularIn',
      name: `Tenant Consumption`,
      type: 'line',
      symbol: 'none',
      silent: true,
      areaStyle: { color: undefined },
      lineStyle: {
        color: undefined,
        width: 0,
      },
      data: [],
    },
  ],
};
