import { ComponentFixture, TestBed } from '@angular/core/testing';

import { CurrentConsumptionComponent } from './current-consumption.component';
import { MonitoringService } from '../../../../services/monitoring.service';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

describe('CurrentConsumptionComponent', () => {
  let component: CurrentConsumptionComponent;
  let fixture: ComponentFixture<CurrentConsumptionComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports: [
        CurrentConsumptionComponent,
        HttpClientTestingModule,
        BrowserAnimationsModule,
      ],
    }).compileComponents();

    fixture = TestBed.createComponent(CurrentConsumptionComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();

    let monitoringService =
      fixture.debugElement.injector.get(MonitoringService);
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
