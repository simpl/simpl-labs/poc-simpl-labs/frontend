import { ComponentFixture, TestBed } from '@angular/core/testing';

import { TestResultBarComponent } from './test-result-bar.component';

describe('TestResultBarComponent', () => {
  let component: TestResultBarComponent;
  let fixture: ComponentFixture<TestResultBarComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports: [TestResultBarComponent],
    }).compileComponents();

    fixture = TestBed.createComponent(TestResultBarComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
