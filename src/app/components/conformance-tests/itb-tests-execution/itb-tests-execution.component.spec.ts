import { ComponentFixture, TestBed } from '@angular/core/testing';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material/dialog';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { Router } from '@angular/router';
import { Observable, of } from 'rxjs';
import { ElementRef } from '@angular/core';
import { By } from '@angular/platform-browser';

import { ITBTestsExecutionComponent } from './itb-tests-execution.component';
import { SlabsCustomComponentService } from '../../../services/slabs-custom-component.service';
import { testExecutionStatus1Mock } from '../../../../mocks/test-execution-status-mock';
import { submittedTestsMock } from '../../../../tests/slabs-custom-components/submitted-test-mock';
import { slabsCustomComponentsMock } from '../../../../mocks/slabs-custom-component-mock';
import { testCasesSuite1Mock } from '../../../../mocks/test-cases-mock';
import { testSuites2Mock } from '../../../../mocks/test-suites-mock';
import { WarningDialogComponent } from '../../_shared/warning-dialog/warning-dialog.component';
import { DialogService } from '../../../services/dialog.service';
import { TestExecutionStatus } from '../../../models/interfaces/slabs-custom-components/test-execution-status.interface';
import {
  itbTestsExecutionDataMock,
  nowMock,
} from '../../../../tests/slabs-custom-components/itb-tests-execution-data-mock';
import { WarningDialogData } from '../../../models/interfaces/warning-dialog-data.interface';
import { TestCaseStatus } from '../../../models/enums/test-case-status.enum';

const dummyElement = document.createElement('div');
document.getElementById = jasmine
  .createSpy('HTML Element')
  .and.returnValue(dummyElement);

class MockMatDialogRef
  implements Partial<MatDialogRef<ITBTestsExecutionComponent>>
{
  close(dialogResult?: any): void {
    return;
  }
  afterClosed(): Observable<any> {
    return of({});
  }
}

class MockWarningDialogRef
  implements Partial<MatDialogRef<WarningDialogComponent<any>>>
{
  close(dialogResult?: any): void {
    return;
  }

  afterClosed(): Observable<any> {
    return of();
  }
}

export class MockElementRef extends ElementRef {
  constructor() {
    super(null);
  }
  override nativeElement: any;
}

describe('ITBTestsExecutionComponent', () => {
  let component: ITBTestsExecutionComponent;
  let fixture: ComponentFixture<ITBTestsExecutionComponent>;
  let slabsCustomComponentService: SlabsCustomComponentService;
  let dialogService: DialogService;
  let router: Router;
  let elementRef: ElementRef<HTMLElement>;

  beforeEach(async () => {
    // mockSlabsCustomComponentService = jasmine.createSpyObj(
    //   'SlabsCustomComponentService',
    //   [],
    // );

    await TestBed.configureTestingModule({
      imports: [
        ITBTestsExecutionComponent,
        HttpClientTestingModule,
        BrowserAnimationsModule,
      ],
      providers: [
        { provide: ElementRef, useClass: MockElementRef },
        { provide: MAT_DIALOG_DATA, useValue: itbTestsExecutionDataMock },
        {
          provide: MatDialogRef,
          useClass: MockMatDialogRef,
        },
        {
          provide: MatDialogRef,
          useClass: MockWarningDialogRef,
        },
      ],
    }).compileComponents();

    router = TestBed.inject(Router);

    fixture = TestBed.createComponent(ITBTestsExecutionComponent);
    component = fixture.componentInstance;

    component.data = itbTestsExecutionDataMock;
    elementRef = fixture.debugElement.query(By.css('#itb-tests-execution-1'));

    component.startTime = nowMock;
    component.endTime = nowMock;
    fixture.detectChanges();

    slabsCustomComponentService = TestBed.inject(
      SlabsCustomComponentService,
    ) as jasmine.SpyObj<SlabsCustomComponentService>;

    dialogService = TestBed.inject(
      DialogService,
    ) as jasmine.SpyObj<DialogService>;

    spyOn(slabsCustomComponentService, 'minimizePage');
    spyOn(slabsCustomComponentService, 'stopTestsExecution');
    spyOn(slabsCustomComponentService, 'getTestSessionReport').and.returnValue(
      of(),
    );
    spyOn(
      slabsCustomComponentService,
      'getTestSuitesBySpecificationId',
    ).and.returnValue(of(testSuites2Mock));
    spyOn(
      slabsCustomComponentService,
      'getTestCasesByTestSuiteId',
    ).and.returnValue(of(testCasesSuite1Mock));
    spyOn(slabsCustomComponentService, 'getCustomComponents').and.returnValue(
      of(slabsCustomComponentsMock),
    );
    spyOn(
      slabsCustomComponentService,
      'getTestExecutionStatus',
    ).and.returnValue(of(testExecutionStatus1Mock));

    spyOn(dialogService, 'openWarningDialog').and.returnValue({
      afterClosed: () => of({}),
    } as MatDialogRef<any, any>);
    spyOn(component.dialogRef, 'afterClosed').and.returnValue(of());
    spyOn(component, 'updateTestExecutionStatus').and.callThrough();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should set #minimized value', () => {
    component.minimized = true;
    submittedTestsMock[0].minimized = false;
    slabsCustomComponentService.submittedTestsSubject$.next(submittedTestsMock);

    expect(component.minimized).toBe(false);
  });

  it('should set #componentId and #specificationId values', () => {
    component.specificationId = undefined!;
    component.componentId = undefined!;
    component.getExecutionStatus();

    expect(component.specificationId).toBe(
      component.data.testExecutionStatus.testExecution.specificationId,
    );
    expect(component.componentId).toBe(
      +component.data.testExecutionStatus.testExecution.componentId,
    );
  });

  it('should update #data.testExecutionStatus and #endTime', () => {
    testExecutionStatus1Mock.testExecution.endDate = nowMock;
    component.updateTestExecutionStatus(testExecutionStatus1Mock);

    expect(component.data.testExecutionStatus).toEqual(
      testExecutionStatus1Mock,
    );
    expect(component.endTime).toBe(
      testExecutionStatus1Mock.testExecution.endDate,
    );
  });

  it('should call service stopTestsExecution()', () => {
    component.stopTestsExecution();

    expect(slabsCustomComponentService.stopTestsExecution).toHaveBeenCalled();
  });

  it('should call service minimizePage()', () => {
    component.minimizeConformanceTests();

    expect(slabsCustomComponentService.minimizePage).toHaveBeenCalled();
  });

  it('should call #openTerminationDialog()', () => {
    spyOn(component, 'openTerminationDialog');

    const testExecutionStatus: TestExecutionStatus = {
      testCasesList: testCasesSuite1Mock.map((x) => {
        x.status = TestCaseStatus.FAILURE;
        return x;
      }),
      testExecution: {
        ...testExecutionStatus1Mock.testExecution,
        startDate: nowMock,
        endDate: nowMock,
      },
    };

    component.updateTestExecutionStatus(testExecutionStatus);

    expect(component.openTerminationDialog).toHaveBeenCalled();
  });

  it('should call getCustomComponents()', () => {
    component.updateCustomComponents(
      component.data.testCases[0],
      component.data.customComponent.id.toString(),
      component.data.testExecutionStatus,
    );

    expect(slabsCustomComponentService.getCustomComponents).toHaveBeenCalled();
  });

  it('should call #dialogRef.afterClosed()', () => {
    slabsCustomComponentService.updateSubmittedTests(submittedTestsMock[0]);

    component.closeDialog();

    expect(component.dialogRef.afterClosed).toHaveBeenCalled();
  });

  it('should call openWarningDialog()', () => {
    const warningDialogData: WarningDialogData<any> = {
      content: 'This is content',
      title: 'This is the title',
    };
    component.submittedTest = submittedTestsMock[0];
    fixture.detectChanges();

    component.openTerminationDialog(warningDialogData);

    expect(dialogService.openWarningDialog).toHaveBeenCalled();
  });

  it('should call getTestSessionReport()', () => {
    component.testCasesList$ = of(testCasesSuite1Mock);
    component.downloadAllReports();

    expect(slabsCustomComponentService.getTestSessionReport).toHaveBeenCalled();
  });

  it('should have #testDialogs.length > 0', () => {
    elementRef = fixture.debugElement.query(By.css('#itb-tests-execution-1'));
    spyOn(elementRef.nativeElement.classList, 'add');
    submittedTestsMock[0].minimized = true;
    slabsCustomComponentService.submittedTestsSubject$.next(submittedTestsMock);

    expect(
      component.testDialogs.get(0)?.nativeElement.classList.add,
    ).toHaveBeenCalled();
    expect(component.testDialogs.length).toBeGreaterThan(0);
  });

  it('should set #submittedTest value', () => {
    spyOn(slabsCustomComponentService, 'updateSubmittedTests');
    slabsCustomComponentService.submittedTests = submittedTestsMock;

    component.updateSubmittedTests(
      submittedTestsMock[0].customComponent,
      submittedTestsMock[0].testExecutionStatus,
    );

    expect(component.submittedTest).toEqual(
      slabsCustomComponentService.submittedTests.find(
        (st) =>
          st.customComponent.id === submittedTestsMock[0].customComponent.id,
      )!,
    );
    expect(component.submittedTest.minimized).toBe(component.minimized);
    expect(component.submittedTest.testExecutionStatus).toEqual(
      submittedTestsMock[0].testExecutionStatus,
    );
    expect(component.submittedTest.testSuites).toEqual(
      submittedTestsMock[0].testSuites,
    );
    expect(component.submittedTest.customComponent).toEqual(
      submittedTestsMock[0].customComponent,
    );
    expect(
      slabsCustomComponentService.updateSubmittedTests,
    ).toHaveBeenCalledWith(component.submittedTest);
  });
});
