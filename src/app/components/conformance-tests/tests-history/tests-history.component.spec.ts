import { ComponentFixture, TestBed } from '@angular/core/testing';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { MatDialog, MatDialogRef } from '@angular/material/dialog';
import { of, Subject } from 'rxjs';

import { TestsHistoryComponent } from './tests-history.component';
import { slabsCustomComponentsMock } from '../../../../mocks/slabs-custom-component-mock';
import { SlabsCustomComponentService } from '../../../services/slabs-custom-component.service';
import {
  testExecutionStatus1Mock,
  testExecutionStatus2Mock,
} from '../../../../mocks/test-execution-status-mock';
import { submittedTestsMock } from '../../../../tests/slabs-custom-components/submitted-test-mock';
import { ITBTestsExecutionComponent } from '../itb-tests-execution/itb-tests-execution.component';
import { SubmittedTest } from '../../../models/interfaces/slabs-custom-components/submitted-test.interface';
import { TestCaseStatus } from '../../../models/enums/test-case-status.enum';

const dialogRefMock: Partial<MatDialogRef<ITBTestsExecutionComponent>> = {
  close: () => {},
  afterClosed: () => of({}),
};

const dialogMock: Partial<MatDialog> = {
  open: () => dialogRefMock as MatDialogRef<ITBTestsExecutionComponent>,
};

describe('TestsHistoryComponent', () => {
  let component: TestsHistoryComponent;
  let fixture: ComponentFixture<TestsHistoryComponent>;
  let customComponentServiceSpy: jasmine.SpyObj<SlabsCustomComponentService>;

  beforeEach(async () => {
    customComponentServiceSpy = jasmine.createSpyObj(
      'SlabsCustomComponentService',
      ['getComponentTestsHistory', 'maximizePage'],
    );

    customComponentServiceSpy.submittedTestsSubject$ = new Subject<
      SubmittedTest[]
    >();

    await TestBed.configureTestingModule({
      imports: [
        TestsHistoryComponent,
        HttpClientTestingModule,
        BrowserAnimationsModule,
      ],
      providers: [
        {
          provide: SlabsCustomComponentService,
          useValue: customComponentServiceSpy,
        },
        {
          provide: MatDialog,
          useValue: dialogMock,
        },
        {
          provide: MatDialogRef,
          useValue: dialogRefMock,
        },
      ],
    }).compileComponents();

    fixture = TestBed.createComponent(TestsHistoryComponent);
    component = fixture.componentInstance;
    component.testsHistory = [];
    component.customComponent = slabsCustomComponentsMock[1];
    component.submittedTest = submittedTestsMock[0];

    customComponentServiceSpy.getComponentTestsHistory.and.returnValue(
      of([testExecutionStatus1Mock, testExecutionStatus2Mock]),
    );

    fixture.detectChanges();
    spyOn(dialogMock, 'open' as any);
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should set #testsHistory value', () => {
    customComponentServiceSpy.getComponentTestsHistory(
      component.customComponent.id,
    );
    expect(component.testsHistory.length).toBeGreaterThan(0);
  });

  it('should set #submittedTest value', () => {
    customComponentServiceSpy.submittedTestsSubject$.next(submittedTestsMock);
    expect(component.submittedTest).toEqual(
      submittedTestsMock.find(
        (st) => st.customComponent.id === component.customComponent?.id,
      )!,
    );
  });

  it('should call customComponentService.maximizePage()', () => {
    component.submittedTest = submittedTestsMock[0];
    const testExecutionMock = { ...testExecutionStatus1Mock };
    testExecutionMock.testExecution.status = TestCaseStatus.ONGOING;
    component.openTestResultSummary(testExecutionMock);
    expect(customComponentServiceSpy.maximizePage).toHaveBeenCalledWith(
      component.submittedTest,
    );
  });
});
