import { ComponentFixture, TestBed } from '@angular/core/testing';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { NotificationsService } from 'angular2-notifications';
import { Subject } from 'rxjs';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

import { ITBTestsExecutionSnackbarComponent } from './itb-tests-execution-snackbar.component';
import { SlabsCustomComponentService } from '../../../services/slabs-custom-component.service';
import { submittedTestsMock } from '../../../../tests/slabs-custom-components/submitted-test-mock';
import { testExecutionStatus1Mock } from '../../../../mocks/test-execution-status-mock';
import { SubmittedTest } from '../../../models/interfaces/slabs-custom-components/submitted-test.interface';

const globalOptions: any = {};

describe('ITBTestsExecutionSnackbarComponent', () => {
  let component: ITBTestsExecutionSnackbarComponent;
  let fixture: ComponentFixture<ITBTestsExecutionSnackbarComponent>;
  let mockNotificationService = new NotificationsService(globalOptions);
  let slabsCustomComponentServiceSpy: jasmine.SpyObj<SlabsCustomComponentService>;

  beforeEach(async () => {
    slabsCustomComponentServiceSpy = jasmine.createSpyObj(
      'SlabsCustomComponentService',
      [
        'updateSubmittedTests',
        'isTestRunning',
        'stopTestsExecution',
        'maximizePage',
      ],
    );

    slabsCustomComponentServiceSpy.submittedTestsSubject$ = new Subject<
      SubmittedTest[]
    >();

    await TestBed.configureTestingModule({
      imports: [
        ITBTestsExecutionSnackbarComponent,
        HttpClientTestingModule,
        BrowserAnimationsModule,
      ],
      providers: [
        { provide: NotificationsService, useValue: mockNotificationService },
        {
          provide: SlabsCustomComponentService,
          useValue: slabsCustomComponentServiceSpy,
        },
      ],
    }).compileComponents();

    fixture = TestBed.createComponent(ITBTestsExecutionSnackbarComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should set #submittedTests value', () => {
    slabsCustomComponentServiceSpy.submittedTestsSubject$.next(
      submittedTestsMock,
    );
    expect(component.submittedTests).toEqual(submittedTestsMock);
  });

  it('should call service.stopTestsExecution()', () => {
    component.submittedTests = submittedTestsMock;
    component.stopTestsExecution(testExecutionStatus1Mock);
    expect(
      slabsCustomComponentServiceSpy.stopTestsExecution,
    ).toHaveBeenCalled();
  });

  it('should call service.maximizePage()', () => {
    component.submittedTests = submittedTestsMock;
    component.maximizeSnackbar(testExecutionStatus1Mock);
    expect(slabsCustomComponentServiceSpy.maximizePage).toHaveBeenCalled();
  });
});
