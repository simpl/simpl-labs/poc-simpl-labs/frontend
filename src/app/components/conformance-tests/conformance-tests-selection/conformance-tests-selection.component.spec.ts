import { ComponentFixture, TestBed } from '@angular/core/testing';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { SimpleChange } from '@angular/core';

import { ConformanceTestsSelectionComponent } from './conformance-tests-selection.component';
import { SlabsCustomComponentService } from '../../../services/slabs-custom-component.service';
import { testSuites1Mock } from '../../../../mocks/test-suites-mock';
import { submittedTestsMock } from '../../../../tests/slabs-custom-components/submitted-test-mock';
import {
  mockTestCases,
  mockSuiteCheckboxes,
} from '../../../../tests/slabs-custom-components/suite-checkboxes-mock';

describe('ConformanceTestsSelectionComponent', () => {
  let component: ConformanceTestsSelectionComponent;
  let fixture: ComponentFixture<ConformanceTestsSelectionComponent>;
  let slabsCustomComponentService: SlabsCustomComponentService;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports: [
        ConformanceTestsSelectionComponent,
        HttpClientTestingModule,
        BrowserAnimationsModule,
      ],
    }).compileComponents();

    const partiallyComplete = 'partiallyComplete';
    const specificationCheckbox = 'specificationCheckbox';

    fixture = TestBed.createComponent(ConformanceTestsSelectionComponent);
    component = fixture.componentInstance;

    slabsCustomComponentService = TestBed.inject(SlabsCustomComponentService);

    component.testSuites = testSuites1Mock;
    component.testCases = mockTestCases;
    component.suiteCheckboxes = mockSuiteCheckboxes;
    component.specificationCheckbox.set({
      id: '2',
      name: 'foo',
      selected: false,
      disabled: false,
      children: mockSuiteCheckboxes,
    });
    component.partiallyComplete();

    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should call #updateCheckboxes() on #onTableCheckboxChange() execution', () => {
    spyOn(component, 'updateCheckboxes');
    const _event = {
      selected: true,
      iSuite: 0,
      iCase: 1,
    };
    component.onTableCheckboxChange(_event);

    expect(component.updateCheckboxes).toHaveBeenCalled();
  });

  it('should call #updateCheckboxes() on #onCheckboxChange() execution', () => {
    spyOn(component, 'updateCheckboxes');
    component.onCheckboxChange(true);

    expect(component.updateCheckboxes).toHaveBeenCalled();
  });

  it('should call #setSpecificationCheckbox() on #ngOnChanges() execution', () => {
    spyOn(component, 'setSpecificationCheckbox');
    const change = new SimpleChange(undefined, submittedTestsMock[0], false);
    component.ngOnChanges({ submittedTest: change });

    expect(component.setSpecificationCheckbox).toHaveBeenCalled();
  });

  it('should call #specificationCheckbox.update() on #updateCheckboxes() execution', () => {
    spyOn(component.specificationCheckbox, 'update');
    component.updateCheckboxes(true, 0, 1);

    expect(component.specificationCheckbox.update).toHaveBeenCalled();
  });

  it('should check all the checkboxes', () => {
    component.updateCheckboxes(true);

    expect(
      component.specificationCheckbox().selected &&
        component
          .specificationCheckbox()
          .children?.every(
            (x) => x.selected && x.children?.every((y) => y.selected),
          ),
    ).toBe(true);
  });

  it('should uncheck all the checkboxes', () => {
    component.updateCheckboxes(false);

    expect(
      component.specificationCheckbox().selected &&
        component
          .specificationCheckbox()
          .children?.every(
            (x) => x.selected && x.children?.every((y) => y.selected),
          ),
    ).toBe(false);
  });

  it('should check test-suite checkboxes', () => {
    component.updateCheckboxes(true, 0);

    expect(component.specificationCheckbox().children![0].selected).toBe(true);
  });

  it('should uncheck test-suite checkboxes', () => {
    component.updateCheckboxes(false, 1);

    expect(component.specificationCheckbox().children![1].selected).toBe(false);
  });

  it('should check test-case checkboxes', () => {
    component.updateCheckboxes(true, 0, 0);

    expect(
      component.specificationCheckbox().children![0].children![0].selected,
    ).toBe(true);
  });

  it('should uncheck test-case checkboxes', () => {
    component.updateCheckboxes(false, 0, 0);

    expect(
      component.specificationCheckbox().children![0].children![0].selected,
    ).toBe(false);
  });

  it('should call #onTestCaseInfoShow.emit() on #openTestCaseInfoDrawer() execution', () => {
    spyOn(component.onTestCaseInfoShow, 'emit');
    component.openTestCaseInfoDrawer(mockTestCases[0][0]);

    expect(component.onTestCaseInfoShow.emit).toHaveBeenCalledWith(
      mockTestCases[0][0],
    );
  });

  it('should call slabsCustomComponentService.isTestRunning() on #setSpecificationCheckbox() execution', () => {
    spyOn(slabsCustomComponentService, 'isTestRunning');
    component.submittedTest = submittedTestsMock[0];
    component.setSpecificationCheckbox();

    expect(slabsCustomComponentService.isTestRunning).toHaveBeenCalled();
  });
});
