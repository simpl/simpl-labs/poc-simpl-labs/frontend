import {
  ComponentFixture,
  ComponentFixtureAutoDetect,
  TestBed,
} from '@angular/core/testing';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { Router, RouterModule } from '@angular/router';
import { of, Subject } from 'rxjs';

import { SLabsCustomComponentsComponent } from './slabs-custom-components.component';
import { SlabsCustomComponentService } from '../../../services/slabs-custom-component.service';
import { SlabsCustomComponentModel } from '../../../models/interfaces/slabs-custom-components/slabs-custom-component.interface';
import { slabsCustomComponentsMock } from '../../../../mocks/slabs-custom-component-mock';
import { routes } from '../../../router/app.routes';
import { MockRouter } from '../../../../tests/helpers/mock-router';
import { submittedTestsMock } from '../../../../tests/slabs-custom-components/submitted-test-mock';

describe('SLabsCustomComponentsComponent', () => {
  let component: SLabsCustomComponentsComponent;
  let fixture: ComponentFixture<SLabsCustomComponentsComponent>;
  let customComponentServiceSpy: jasmine.SpyObj<SlabsCustomComponentService>;
  let mockRouter = new MockRouter();

  beforeEach(async () => {
    customComponentServiceSpy = jasmine.createSpyObj(
      'SlabsCustomComponentService',
      ['loadCustomComponents', 'getCustomComponents'],
    );

    customComponentServiceSpy.customComponentsSubject$ = new Subject<
      SlabsCustomComponentModel[]
    >();

    customComponentServiceSpy.submittedTests = submittedTestsMock;
    customComponentServiceSpy.customComponents = slabsCustomComponentsMock;

    await TestBed.configureTestingModule({
      imports: [
        SLabsCustomComponentsComponent,
        HttpClientTestingModule,
        BrowserAnimationsModule,
        RouterModule.forRoot(routes),
      ],
      providers: [
        {
          provide: SlabsCustomComponentService,
          useValue: customComponentServiceSpy,
        },
        {
          provide: Router,
          useValue: mockRouter,
        },
        { provide: ComponentFixtureAutoDetect, useValue: true },
      ],
    }).compileComponents();

    fixture = TestBed.createComponent(SLabsCustomComponentsComponent);
    component = fixture.componentInstance;

    spyOn(
      customComponentServiceSpy.customComponentsSubject$,
      'asObservable',
    ).and.returnValue(of(slabsCustomComponentsMock));

    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should go to ConformanceTests Component', () => {
    spyOn(mockRouter, 'navigateByUrl');
    component.goToConformanceTests(slabsCustomComponentsMock[0].id);
    expect(mockRouter.navigateByUrl).toHaveBeenCalled();
  });

  it('should not call customComponentsSubject$.next()', () => {
    const { customComponents } = customComponentServiceSpy;
    expect(component.customComponents).toEqual(customComponents);
  });

  it('should call customComponentsSubject$.next()', () => {
    customComponentServiceSpy.customComponents = undefined!;
    component.ngOnInit();
    customComponentServiceSpy.customComponentsSubject$.next(
      slabsCustomComponentsMock,
    );
    expect(
      customComponentServiceSpy.customComponentsSubject$.asObservable,
    ).toHaveBeenCalled();
  });

  it('should set #selectedComponent value', () => {
    spyOn(component.onComponentSelect, 'emit');
    component.selectComponent(slabsCustomComponentsMock[0]);
    expect(component.selectedComponent).toEqual(slabsCustomComponentsMock[0]);
    expect(component.onComponentSelect.emit).toHaveBeenCalled();
  });
});
