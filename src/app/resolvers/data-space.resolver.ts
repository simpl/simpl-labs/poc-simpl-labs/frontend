import { inject } from '@angular/core';
import { ActivatedRouteSnapshot, ResolveFn } from '@angular/router';

import { DataSpaceModel } from '../models/interfaces/data-space/data-space.interface';
import { DataSpaceService } from '../services/data-space.service';

export const dataSpaceResolver: ResolveFn<DataSpaceModel> = (
  route: ActivatedRouteSnapshot,
) => inject(DataSpaceService).getDataSpace(+route.paramMap.get('id')!);
