export const createMouseEvent = (): MouseEvent => {
  const event = {
    stopPropagation: () => undefined,
  };
  return event as MouseEvent;
};
